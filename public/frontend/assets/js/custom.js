$(document).ready(function(){

    // Accordion


    $('.pannel-box-wrap').hide();
    $('.pannel-active').show();

    $('.account-pannel').on('click',function(){
      var newPannel = $(this).attr('data-idPannel');
      $('#'+newPannel).toggle('medium');
      $('.pannel-box-wrap').not($(this).next()).slideUp('medium');
    });

});