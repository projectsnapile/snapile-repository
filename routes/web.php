<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Frontend routes

// Route::resource('/', 'IndexController');
// Route::get('/faq', function() {
// 	return view('layouts/frontend/pages/faq');
// });
// Route::get('/about', function() {
// 	return view('layouts/frontend/pages/aboutus');
// });
// Route::get('/terms', function() {
// 	return view('layouts/frontend/pages/terms');
// });
// Route::get('/faq', function() {
// 	return view('layouts/frontend/pages/faq');
// });
// Route::get('/blog', 'BlogController@blogpage');
// Route::get('blog-detail/{blog}','BlogController@blogdetail');
// Route::get('/writeblog', 'BlogController@create');
// Route::get('/upload', 'UploadController@create');
// Route::get('/discover', 'UploadController@discoverpage');

	




// Route::middleware(['auth'])->group(function () {

// 	Route::resource('admin/dashboard/category', 'CategoryController');
// 	Route::get('admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
// });


// Route::resource('/admin/dashboard/imageupload', 'UploadController')->except(['create']);
// Route::resource('/admin/dashboard/blog', 'BlogController')->except(['create']);
// Route::post('addtag', 'CategoryController@createtag');
// Route::post('/changestatus','UploadController@changestatus');
// Route::post('/blogstatus','BlogController@blogstatus');


// Route::get('/home', 'HomeController@index')->name('home');
// Auth::routes();














/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Frontend routes

Route::resource('/', 'IndexController');
Route::get('/faq', function() {
	return view('layouts/frontend/pages/faq');
});
Route::get('/about', function() {
	return view('layouts/frontend/pages/aboutus');
});
Route::get('/terms', function() {
	return view('layouts/frontend/pages/terms');
});
Route::get('/faq', function() {
	return view('layouts/frontend/pages/faq');
});
Route::get('/blog', 'BlogController@blogpage');
Route::get('blog-detail/{blog}','BlogController@blogdetail');
Route::get('/portfolio/writeblog', 'BlogController@create');
Route::post('/portfolio', 'BlogController@store');
Route::get('/admin/writeblog', 'BlogController@admincreate');
Route::get('/upload', 'UploadController@create');
Route::get('/discover', 'UploadController@discoverpage');
Route::get('/image/{upload}/edit','UploadController@edit');
Route::post('/imageedit', 'UploadController@updateuserimage');
Route::get('/image/{upload}/download', 'DownloadController@index')->name('image.download');
Route::get('/user/login','\Snapile\Http\Controllers\Auth\LoginController@userloginpage')->name('user.login');
Route::post('/user/register','\Snapile\Http\Controllers\Auth\RegisterController@register');

Route::get('/payment/verification', 'DownloadController@verification');

Route::get('/user/{id}/viewprofile','HomeController@viewprofile');
Route::get('/portfolio', 'HomeController@index')->name('portfolio');
Route::post('/portfolio/{id}', 'HomeController@profileimagestore');
// ->middleware(['auth','checkrole'])
//  Route::get('admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
//  Route::post('admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

	
	
Route::get('/image/search','UploadController@search');
Route::resource('/admin/dashboard/imageupload', 'UploadController')->except(['create']);
Route::post('/changestatus','UploadController@changestatus');
Route::post('addtag', 'CategoryController@createtag');
Route::post('/user/{id}/sendmessage', 'HomeController@usermessage');
Route::post('/changeusermessagestatus/{message}','HomeController@updatestatus');
Route::group(['middleware' => ['auth','admin']], function () {
	Route::resource('admin/dashboard/category', 'CategoryController');
	Route::resource('/admin/dashboard/blog', 'BlogController')->except(['create','store']);
	Route::post('/blogstatus','BlogController@blogstatus');
	Route::post('/admin/sendmessage', 'UploadController@sendmessage');
});
Route::get('admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard');


Auth::routes();

























