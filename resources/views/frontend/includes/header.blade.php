<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Snapile-Stock Photo Library</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/bootstrap-grid.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/bootstrap-reboot.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/aos.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/dist/css/lightbox.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/dist/css/lightbox.min.css') }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/style.css') }}">
 
	<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900|Sniglet:400,800" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/vendor/select2/select2.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
	<!--preloader-->
<!-- <div class="sk-folding-cube">
  <div class="sk-cube1 sk-cube"></div>
  <div class="sk-cube2 sk-cube"></div>
  <div class="sk-cube4 sk-cube"></div>
  <div class="sk-cube3 sk-cube"></div>
</div> -->
<!--preloader ends here-->

</head>

<!--Main Navigation-->
<header class="header-main">


<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">

<div class="container">

    <a class="navbar-brand" href="{{ url('/') }}">
          <img src="{{ asset('frontend/assets/logo.png') }}" width="140px" height="60px" alt="snapile logo">
        </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-home">
          <a class="nav-link" href="{{ url('/') }}">Home
                <span class="sr-only">(current)</span>
              </a>
        </li>
        <li class="nav-blog">
          <a class="nav-link" href="{{ url('blog') }}">Blog</a>
        </li>
        <li class="nav-discover">
          <a class="nav-link" href="{{ url('discover') }}">Discover</a>
        </li>
        <li class="nav-more">
          <a class="nav-link" href="#">
          <div class="dropdown show">
  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
{{Auth()->check()?Auth()->user()->name:'ACTION'}}
  </a>
@if(Auth()->check())
  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="{{ url('portfolio') }}">My profile</a>
<form method="POST" action="{{route('logout')}}">
  {{csrf_field()}}
    <button class="dropdown-item">logout</button>
</form>
  </div>
  @else
  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="{{ route('user.login') }}">Login</a>
    <a class="dropdown-item" href="{{ route('user.login') }}">Register</a>
</div>
@endif
</div>
        </a>
        </li>
        <li class="nav-more">
          <a class="nav-link" href="#">
          <div class="dropdown show">
  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    More
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="{{ url('faq') }}">FAQ</a>
    <a class="dropdown-item" href="{{ url('about') }}">About Us</a>
    <a class="dropdown-item" href="{{ url('terms') }}">Terms & Conditions</a>
  </div>
</div>
        </a>
        </li>
      </ul>

      </div>
</nav>

</header>