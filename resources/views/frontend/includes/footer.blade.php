<!----------------------------
-------Footer-------
----------------------------->


<footer class="footer-main">

    <div class="footer-top">

        <div class="container"> <!-- Container .// -->
        
            <div class="row"> <!-- Row .// -->
            
                <div class="col-lg-4"> <!-- Col .// -->
                
                    <div class="foot-wrap">
                        <h5 class="footer-title">Follow us</h5>
                        <ul class="footer-social">
                            <li><a href="#" class="footer-social-link"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>

                </div> <!-- Col .// -->

                <div class="col-lg-4"> <!-- Col .// -->

                    <div class="foot-wrap footer-box">
                        <div class="text-center">
                            <a class="navbar-brand" href="{{ url('/') }}"><span>Snapile</span></a>

                            <p class="footer-content">Snapile is a growing stock photography platform and a reliable supplier of high-quality digital images. That’s a nice word to describe certain platforms, but we are lot more than that. Snapile is the stock photography site based in Nepal.</p>
                        </div>
                    </div>
                
                </div> <!-- Col .// -->

                <div class="col-lg-4"> <!-- Col .// -->

                    <div class="foot-wrap">
                        <h5 class="footer-title">Newsletter</h5>

                        <div class="newsletter">
                            <form action="newsletter">
                                <input type="email" placeholder="Subscribe To Our Newsletter" class="newsletter-box" required>
                                <button class="news-submit">Join</button>
                            </form>
                        </div>
                    </div>
                
                </div> <!-- Col .// -->

            </div> <!-- Row .// -->

        </div> <!-- Container .// -->

    </div>

    <div class="footer-bottom">
        <div class="to-top">
            <button id="to-top-btn" onclick="gototop()"><i class="fa fa-angle-double-up" aria-hidden="true"></i></button>
        </div>
        <div class="container"> <!-- Container .// -->

            <!-- <div class="bottom-contact-info" data-aos="fade-left"> -->
            <div class="bottom-contact-info">
                
        
            </div>
            <div class="bottom-copyright"><p class="footer-content text-center copyright">Copyright &copy; 2019, All Rights Reserved by: <a href="{{ url('/') }}" target="_blank">Snapile</a></p></div>
        </div> <!-- Container .// -->
    </div>

</footer>

<!----------------------------
-------Footer-------
----------------------------->
