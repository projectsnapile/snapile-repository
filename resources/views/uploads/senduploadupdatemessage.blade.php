@component('mail::message')
Hello {{$name}},

Regarding <strong>{{$upload}}</strong> (uploaded on {{$uploadeddate}})
<br>
{{$message}}
<br>


Thanks and regards,<br>
Snapile Team
@endcomponent
