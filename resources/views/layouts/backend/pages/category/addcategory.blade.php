@extends('layouts.backend.welcome')
 @section('main-content')

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Form-->
                <!-- ============================================================== -->
                 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                @endif

                @if(Session()->has('message'))
                <p class="alert alert-success">
                {{ Session()->get('message') }}
                </p>
                @endif
                

                 <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">Add Category</h4>
                            <h5 class="card-subtitle"> Image Category </h5>
                            <form class="form-horizontal m-t-30" role="form" method="post" action="{{ url('admin/dashboard/category') }}">
                                 
                                 {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Category Name </label>
                                    <input type="text" class="form-control" name="cat_name" id="cat_name">
                                </div>
                                     <input type="hidden" name="tags"  id="demo">
                                 <div class="form-group" id="tagForm">
                                    <label>Tags </label>
                               <!--      <select multiple="multiple" name="tag[]" id="tag" class="form-control js-example-basic-multiple">
                                   
                                    </select>
                                    tagForm"> -->
                                                                                                
                            <select multiple="true" name="tag[]" id="choosetag" class="form-control select2" style="height: 36px;width: 100%;">

                             @foreach($tags as $t)
                             <option value="{{$t->name}}">{{ $t->name }}</option>
                                    @endforeach
    
                            </select>
                                                                
                                </div>
                          <input type="Submit" name="submit" value="Submit" class="btn btn-success btn-md">
                         </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Form -->
                <!-- ============================================================== -->
                
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
           
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    

</body>
<script type="text/javascript">
    $('#tag').select2({
    tags: true,
    tokenSeparators: [",", " "]
});

$('#tagForm').submit(function(e){
    e.preventDefault();
    $.ajax({
        url: "{{ url('admin/dashboard/category') }}",
        type: 'post',
        dataType: 'html',
        data: $('#tagForm').serialize(),
        
    });
});
</script>

</html>

@endsection