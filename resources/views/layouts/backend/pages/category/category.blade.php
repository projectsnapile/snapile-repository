
@extends('layouts.backend.welcome')
 @section('main-content')

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Form-->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title ">Category</h4>
                                <h6 class="card-subtitle d-inline-block">Manage your Image category</h6>
                                <a href="{{ url('admin/dashboard/category/create')  }}"><button class="btn btn-info btn-md float-right btn-round">Add New Category</button></a>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">S.N.</th>
                                            <th scope="col">Category Name</th>
                                            <th scope="col">Tags</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($categories as $c)
                                        <tr>
                                            <th scope="row">{{ $c -> id }}</th>
                                            <td>{{ $c -> cat_name }}</td>
                                            <td>@foreach($c -> tags as $tag)
                                                {{$tag->name}}
                                                @endforeach
                                            </td>
                                            <td><a href="{{url('admin/dashboard/category/' .$c -> id. '/edit') }}" class="btn btn-success btn-sm"><i class="fas fa-edit" style="font-size: 0.8rem;"></i></a>
                                            {!!Form::open([
                                                'method' =>'DELETE',
                                                'class' => 'd-inline-block',
                                                'route' => ['category.destroy', $c -> id], 
                                                'onsubmit' => 'return confirm("Are you sure you want to delete??")'
                                                ]) !!}
                                            {{ Form::button('<i class="fa fa-trash-alt"  style="font-size: 1rem;"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] )  }}
                                            {!! Form::close(); !!} </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pagination justify-content-center">
                                {!! $categories -> links()!!}
                        </div>
                    </div>
                     </div>
                <!-- ============================================================== -->
                <!-- Form -->
                <!-- ============================================================== -->
                
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
           
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    

</body>

</html>

@endsection