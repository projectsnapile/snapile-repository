@extends('layouts.backend.welcome')
 @section('main-content')

 <body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="container-fluid">
	            <div class="page-breadcrumb">
	                <div class="row">
                        <div class="col-5 align-self-center">
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
	                    <div class="col-7 align-self-center">
	                        <div class="d-flex no-block justify-content-end align-items-center">
	                            <nav aria-label="breadcrumb">
	                                <ol class="breadcrumb">
	                                    <li class="breadcrumb-item">
	                                        <a href="{{ url('admin/dashboard/blog') }}">Blogs</a>
	                                    </li>
	                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
	                                </ol>
	                            </nav>
	                        </div>
	                    </div>
	                </div>
	            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
           
            <!-- Edit Form Starts Here -->

             <!-- .row -->
              @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                        @endif

                        @if(Session()->has('message'))
                        <p class="alert alert-success">
                        {{ Session()->get('message') }}
                        </p>
                        @endif

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">Edit Blog</h4>
                            <h5 class="card-subtitle">{{$blogs -> b_title}} </h5>
                            <form class="form-horizontal m-t-30" id="blogedit-form" method="post" enctype="multipart/form-data" action="{{ url('admin/dashboard/blog/' .$blogs -> id) }}" role="form">
                                <input type="hidden" name="_method" value="put">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="b_title">Title</label>
                                    <input type="text" id="b_title" name="b_title" class="form-control" value="{{$blogs -> b_title}}">
                                </div>
                               
                                <div class="form-group">
                                    <label>Author</label>
                                    <input class="form-control" type="text" value="{{$blogs -> b_author}}" readonly>
                                </div>
                                
                               <div class="form-group">
                                    <label>Upload Image</label>
                                    <input type="file" src="{{ asset('images/blogs'.$blogs -> image) }}" name="blogpic" class="form-control"  accept="image/*">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                <textarea id="mymce" name="b_detail" >{{$blogs->b_detail}}</textarea>
                                </div>
                                 <input type="Submit" name="submit" value="Update" class="btn btn-info btn-md">
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            <!-- Edit Form Ends Here -->


            </div>
        </div>

</body>

</html>

@endsection