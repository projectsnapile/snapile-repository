@extends('layouts.backend.welcome')
 @section('main-content')

 <body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="container-fluid">
	            <div class="page-breadcrumb">
	                <div class="row">
	                    <div class="col-5 align-self-center">
	                        <h4 class="page-title">Pending Blogs</h4>
	                        <div class="d-flex align-items-center">

	                        </div>
	                    </div>
	                    <div class="col-7 align-self-center">
	                        <div class="d-flex no-block justify-content-end align-items-center">
	                            <nav aria-label="breadcrumb">
	                                <ol class="breadcrumb">
	                                    <li class="breadcrumb-item">
	                                        <a href="#">Home</a>
	                                    </li>
	                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
	                                </ol>
	                            </nav>
	                        </div>
	                    </div>
	                </div>
	            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- Row -->
                <div class="row">
                    <div class="col-12 m-t-30">
                        <a href="{{ url('admin/writeblog')  }}"><button class="btn btn-info btn-md float-right btn-round">Add New Blog</button></a><br><br>
                        <div class="card-group">
                        	@foreach ($blogs as $b) 
                            <div class="col-6">
                            <div class="card">
                                <img class="card-img-top img-responsive" src="{{ asset('images/blogs/'.$b -> image) }}" alt="image" height="310px">
                                <div class="card-body">
                                    <h4 class="card-title">{{$b -> b_title}}</h4>
                                    <h6 class="card-text">{{$b->user->name}}</h6>
                                    <p class="card-text"><small class="text-muted">Last updated {{$b -> created_at->format('Y-m-d')}}</small></p>
                                    <p class="card-text">{{ str_limit($b -> b_detail, $limit = 300, $send = '...') }}</p>
                                </div>
                                <div class="action-btn">
                                <a href="{{url('admin/dashboard/blog/' .$b -> id. '/edit') }}" class="btn btn-success btn-sm"><i class="fas fa-edit" style="font-size: 1rem;"></i></a>
                                {!!Form::open([
                                                'method' =>'DELETE',
                                                'class' => 'd-inline-block',
                                                'route' => ['blog.destroy', $b -> id], 
                                                'onsubmit' => 'return confirm("Are you sure you want to delete??")'
                                                ]) !!}
                                            {{ Form::button('<i class="fa fa-trash-alt"  style="font-size: 1rem;"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] )  }}
                                            {!! Form::close(); !!} 

                                            <div class="d-inline-block" id="b_status{{$b->id}}">
    @if($b->b_status===1)
    <button class="button btn-success" onclick="blogstatus('{{$b->id}}')">Published</button>
    @else
      <button class="button btn-danger"  onclick="blogstatus('{{$b->id}}')">UnPublished</button>
    @endif
</div>
                                </div>        
                            </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
        </div>

</body>

</html>

@endsection