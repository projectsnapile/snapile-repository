@extends('layouts.backend.welcome')
 @section('main-content')

 <body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                @endif

                @if(Session()->has('message'))
                <p class="alert alert-success">
                {{ Session()->get('message') }}
                </p>
                @endif
            <div class="createblog">
    <h2>Create Blog</h2>
    <hr>
    <form id="contact-form" method="post" enctype="multipart/form-data" action="{{ url('/portfolio') }}" role="form">
    {{ csrf_field() }}
    <div class="form-group">
    <label for="exampleInputtitle">Title</label>
    <input class="form-control" id="b_title" type="text" name="b_title" placeholder="Enter your title">
  </div>
  <div class="form-group">

  <input id="b_author" type="hidden" name="b_author" value="{{Auth::user()->id}}" class="form-control">
  </div>
  <div class="form-group">
    <label for="exampleInputDescription">Description</label>
    <!-- <input type="Description" class="form-control" id="exampleInputdescription1" aria-describedby="descriptionhelp" placeholder="Enter your Description">  -->
  <textarea id="b_detail" type="text" name="b_detail" class="form-control" placeholder="Enter your Description" cols="90" rows="5"></textarea>
  </div>
  <div class="form-group">
      <label>Upload Image</label>
      <input type="file" name="blogpic" class="form-control"  accept="image/*">
  </div>
  <input type="Submit" name="submit" value="Submit" class="btn btn-primary btn-md">
</form>
</div>
</div>


</div>

</body>

</html>

            @endsection