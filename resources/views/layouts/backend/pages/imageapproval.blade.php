@extends('layouts.backend.welcome')
 @section('main-content')

 <body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="container-fluid">
	            <div class="page-breadcrumb">
	                <div class="row">
	                    <div class="col-5 align-self-center">
	                        <h4 class="page-title">Image Approval</h4>
	                        <div class="d-flex align-items-center">

	                        </div>
	                    </div>
	                    <div class="col-7 align-self-center">
	                        <div class="d-flex no-block justify-content-end align-items-center">
	                            <nav aria-label="breadcrumb">
	                                <ol class="breadcrumb">
	                                    <li class="breadcrumb-item">
	                                        <a href="#">Home</a>
	                                    </li>
	                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
	                                </ol>
	                            </nav>
	                        </div>
	                    </div>
	                </div>
	            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- Row -->
        
                <!-- End Row -->

                <br><br>
                <div class="row el-element-overlay">
                   @foreach ($uploads as $u) 
                    <div class="col-lg-4 col-md-6">    
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1"> <img src="{{ asset('images/watermark/'.$u -> image) }}" alt="image" />
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="{{ asset('images/'.$u -> image) }}"><i class="sl-icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="javascript:void(0);"><i class="sl-icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                 <div class="card-body">
                                    <h4 class="card-title">{{$u -> p_title}}</h4>
                                    <p class="card-text">Uploaded by: {{$u -> user->name}}</p>
                                    <p class="card-text">{{$u -> p_desc}}</p>
                                    <p class="card-text"><small class="text-muted">Last updated {{$u -> created_at}}</small></p>
                                    <p class="card-text"><strong>Exif Info:</strong> <br>
                                        <?php 
                          $exif = json_decode($u->p_exif, true);
                          
                          echo "Resolution: " . $exif['Resolution']; echo "<br>";
                          echo "Model: " . $exif['Model']; echo "<br>";
                          echo "Snap Info: " . $exif['SnapInfo']; echo "<br>";
                          
                                   

?>  
                                    </p>
                            <p class="card-text"><strong>Tags:</strong></p>
                            <ul class="tagsname">
                            @foreach($u->tags as $tag)
                             <li><a href="#" class="tagdesign">{{$tag->name}}</a></li>
                             @endforeach
                            </ul>
                            <br>
                            <p class="card-text"><strong>Actions:</strong></p>
                                <div class="img-action row">  
                                 <div id="status{{$u->id}}" class="pr-2">
                                    @if($u->status===1)
                                    <button class="btn btn-success" onclick="changestatus('{{$u->id}}')">Published</button>
                                    @else
                                      <button class="btn btn-danger"  onclick="changestatus('{{$u->id}}')">Unpublished</button>
                                    @endif
                                </div>
                                {!!Form::open([
                                    'method' =>'DELETE',
                                    'class' => 'd-inline-block',
                                    'route' => ['imageupload.destroy', $u -> id], 
                                    'onsubmit' => 'return confirm("Are you sure you want to delete??")'
                                    ]) !!}
                                {{ Form::button('Delete', ['type' => 'submit', 'class' => 'btn btn-danger'] )  }}
                                {!! Form::close(); !!} 
                                    <div class="pl-2">
                                <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sendmessage{{$u->id}}">
Send Mail
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="sendmessage{{$u->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Send email </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="/admin/sendmessage">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{$u->user->email}}" name="email">
                                            <input type="hidden" value="{{$u->p_title}}" name="upload">
                                            <input type="hidden" value="{{$u->created_at}}" name="uploadeddate">
                                            <input type="hidden" value="{{$u->user->name}}" name="name">
                                        <textarea name="message" rows="10" cols="63"></textarea>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Send</button>
                                </form>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                           
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
                                    <a href="/image/{{$u->id}}/edit" class="btn btn-primary">Edit</a>

                               </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>



            </div>
        </div>

</body>

</html>

@endsection