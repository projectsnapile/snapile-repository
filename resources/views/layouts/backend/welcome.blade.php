@include('backend.includes.header')

@include('backend.includes.sidenav')

    @yield('main-content')

@include('backend.includes.footer')