
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>TEST</title>
  </head>
  <body>
    <section class="page-form">
    <div class="container">

            <div class="col-md-7 col-sm-7 form-area" data-aos="zoom-in">


                <h4>Image Upload Test</h4>
                 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                @endif

                @if(Session()->has('message'))
                <p class="alert alert-success">
                {{ Session()->get('message') }}
                </p>
                @endif

                    <form id="contact-form" method="post" enctype="multipart/form-data" action="{{ url('admin/dashboard/imageupload') }}" role="form">
                        {{ csrf_field() }}
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="p_title">Title </label>
                                        <input id="p_title" type="text" name="p_title" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="p_desc">Description </label>
                                        <input id="p_desc" type="text" name="p_desc" class="form-control">
                                    </div>
                                </div>
                            </div>
                           <!--  <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="p_desc">Tag </label>
                                        <input id="tag" type="text" name="tag" class="form-control">
                                    </div>
                                </div>
                            </div> -->
                            
                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="category_id">Category </label>
                                        <select id="category_id" name="category_id" class="form-control">
                                            @foreach($categories as $c)
                                                <option value="{{$c -> id}}">{{$c -> cat_name}}</option>
                                            @endforeach
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Upload Image</label>
                                        <input type="file" name="pic" class="form-control"  accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <input type="Submit" name="submit" value="Submit" class="btn btn-success btn-md">
                        </div>

                    </form>

            </div>
        </div>
    </div>
</section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
