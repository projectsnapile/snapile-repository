
@extends('layouts.frontend.welcome')
 @section('main-content')
<body>
<div class="discover-page">
    <!----------------------------
-------Breadcrumb-------
----------------------------->

<section class="banner bg-para" style="background-image: linear-gradient(to top, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.3)), url('{{asset('frontend/img/bg-img/18.jpg')}}');">   
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->
            <div class="col-md-6"> <!-- Col .// -->
                <h3>Search Image</h3>
            </div> <!-- Col .// -->
        </div>  <!-- Row .// -->

        <div class="u-margin-top-small">
            <div class="row"> <!-- Row .// -->

                <div class="col-lg-8 offset-lg-2"> <!-- Col .// -->

                    <div class="uni-title">
                        <h5 class="color-white text-center"><i class="fa fa-search" aria-hidden="true"></i> Search Images</h5>
                    </div>

                    <form action="search" method="GET" action="/image/search">
                        <div class="row"> <!-- Inner Row .// -->
                            <div class="col-md-10"> <!-- Inner Col .// -->
                                <div class="input-area clearfix">
                                    <div class="input-group-prepend float-left">
                                        <span class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></span>
                                    </div>
                                    <input type="text" name="searchkey" placeholder="Want a HQ Picture?" required>
                                </div>
                            </div> <!-- Inner Col .// -->
                            <div class="col-md-2"> <!-- Inner Col .// -->

                                <div class="input-area">
                                    <button>&rarr;</button>
                                </div>
                            
                            </div> <!-- Inner Col .// -->
                        </div> <!-- Inner Row .// -->
                    </form>
                
                </div> <!-- Col .// -->
            </div> <!-- Row .// -->
        </div>

    </div> <!-- Container .// -->
</section>

<!----------------------------
-------Breadcrumb-------
----------------------------->


<!----------------------------
-------Photogrid-------
----------------------------->
<!-- <div class="grid">
  <div class="grid-item"><img src="img/bg-img/8.jpg" alt="image" ></div>
  <div class="grid-item grid-item--width2"><img src="img/bg-img/9.jpg" alt="image" ></div>
  <div class="grid-item"><img src="img/bg-img/10.jpg" alt="image" ></div>
</div> -->

<div class="discoverpage container-fluid">
	<h2>Discover</h2>
	<section class="feature-destination">
      <div class="container">
        <div class="row">
@if(count($data)>0)
@if($data[0]->uploads)
        @foreach($data[0]->uploads as $up)
          <div class="col-md-4 element-animate ">
            <a href="{{route('image.download', $up->id)}}" class="img-bg" style="background-image: url('{{asset('images/thumbnails/'.$up -> image)}}')">
              <div class="text">
                <h2>{{$up->p_title}}</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
        @endforeach
        @else
        <h3 class="text-danger text-center">{!!$message!!}</h3>
        @endif
        @else
        <h3 class="text-danger text-center">{!!$message!!}</h3>
        @endif
        <!-- <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="download.php" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="download.php" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="download.php" class="img-bg last" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="download.php" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="download.php" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="download.php" class="img-bg last" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
        </div> -->
      </div>
    </section>

</div>  

<!-- categories Section -->
</div>



</body>
</html>
@endsection