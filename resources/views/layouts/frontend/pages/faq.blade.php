
@extends('layouts.frontend.welcome')
 @section('main-content')
<body>
<div class="faq">
    
<!----------------------------
-------Breadcrumb-------
----------------------------->

<section class="banner bg-para" style="background-image: linear-gradient(to top right, rgba(5, 25, 34, 0.3), rgba(10, 38, 51, 0.3)), url(img/bg-img/3.jpg);">   
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->
            <div class="col-md-6"> <!-- Col .// -->
                <h3>FAQs</h3>
            </div> <!-- Col .// -->
        </div>  <!-- Row .// -->
    </div> <!-- Container .// -->
</section>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<!----------------------------
-------FAQ-Main-------
----------------------------->

<section class="faq-main common-padding">
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->
            <div class="col-lg-9"> <!-- Col .// -->

            <!--Box(START)-->
            <div class="account-title-box">
                <p href="" class="account-pannel" data-idPannel="pannel1">Q.1: <span class="down-arr">Lorem ipsum dolor sit amet?</p>
            <div class="pannel-box-wrap pannel-active" id="pannel1">
                <p class="normal-content"><strong>Ans:</strong> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut laudantium soluta amet repellendus quidem, dignissimos earum sequi minus, obcaecati ad consequuntur distinctio! Repellat vel eligendi pariatur fuga laborum. Molestiae, quia totam? Excepturi, velit? Mollitia dignissimos blanditiis itaque reiciendis molestias, impedit eum veniam necessitatibus magnam aliquid!</p>
            </div>
            </div>
            <!--Box(END)-->

            <!--Box(START)-->
            <div class="account-title-box">
                <p href="" class="account-pannel" data-idPannel="pannel2">Q.2: <span class="down-arr">Lorem ipsum dolor sit?</p>
            <div class="pannel-box-wrap" id="pannel2">
                <p class="normal-content"><strong>Ans:</strong> Lorem ipsum dolor sit amet consectetur adipisicing elit. Error saepe aspernatur quae voluptatum dolores ab corrupti a. Nobis, omnis illo. Eius earum nesciunt ipsam, vero labore veniam, quia veritatis magnam est reprehenderit atque, nostrum reiciendis.</p>
            </div>
            </div>
            <!--Box(END)-->

            <!--Box(START)-->
            <div class="account-title-box">
                <p href="" class="account-pannel" data-idPannel="pannel3">Q.3: <span class="down-arr">Lorem ipsum dolor sit amet consectetur?</p>
            <div class="pannel-box-wrap" id="pannel3">
                <p class="normal-content"><strong>Ans:</strong> Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure illo pariatur velit! Magnam blanditiis harum rerum non accusantium laudantium officiis vitae numquam id fugit. Aspernatur sunt iure eveniet quam veniam.</p>
            </div>
            </div>
            <!--Box(END)-->

            <!--Box(START)-->
            <div class="account-title-box">
                <p href="" class="account-pannel" data-idPannel="pannel4">Q.4: <span class="down-arr">Lorem ipsum dolor sit amet?</p>
            <div class="pannel-box-wrap" id="pannel4">
                <p class="normal-content"><strong>Ans:</strong> Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis cum tempora nobis provident! Nulla beatae quos id eaque, ipsum libero rem hic aliquam enim laudantium?</p>
            </div>
            </div>
            <!--Box(END)-->

            <!--Box(START)-->
            <div class="account-title-box">
                <p href="" class="account-pannel" data-idPannel="pannel5">Q.5: <span class="down-arr">Lorem ipsum dolor sit amet?</p>
            <div class="pannel-box-wrap" id="pannel5">
                <p class="normal-content"><strong>Ans:</strong> Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis cum tempora nobis provident! Nulla beatae quos id eaque, ipsum libero rem hic aliquam enim laudantium?</p>
            </div>
            </div>
            <!--Box(END)-->
            
            </div> <!-- Col .// -->
            </div> <!-- Col .// -->
        </div> <!-- Row .// -->
    </div> <!-- Container .// -->
</section>

<!----------------------------
-------FAQ-Main-------
----------------------------->
</div>
</body>
</html>
@endsection