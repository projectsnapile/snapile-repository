
@extends('layouts.frontend.welcome')
 @section('main-content')
<body>
<!---------------------
----Landing-Section---
---------------------->

   <!-- <section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(assets/mats/big_image_1.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-8 text-center">

            <div class="mb-5 element-animate">
              <h1>Stock Photo Library</h1>
              <p>Discover various of Free & premium images at one place.</p>
            </div>

            <form class="form-inline element-animate" id="search-form">
              
              <input type="text" class="form-control form-control-block search-input" id="autocomplete" placeholder="Search Stock Images by Keywords">
              <button type="submit" class="btn btn-primary">Search</button>
            </form>

          </div>
        </div>
      </div>
    </section> -->
    <!-- END section -->
    
<section class="landing-section">
  <div class="bg-video">
    <video class="bg-video__content" autoplay muted loop>
      <source src="{{ asset('frontend/img/video/cam.mp4') }}" type="video/mp4">
      <source src="{{ asset('frontend/img/video/cam.webm') }}" type="video/webm">
      Your browser is not supported!
    </video>
  </div>

  <div class="landing-overlay text-center color-white">

    <div class="mb-5 element-animate">
      <h1>Stock Photo Library</h1>
      <p class="aos-init aos-animate" data-aos="fade-left">Discover a blend of Free & Premium images at one place.</p>
    </div>
    <form class="form-inline element-animate" id="search-form" method="GET" action="/image/search">
      
      <input type="text" class="form-control form-control-block search-input" id="autocomplete" name="searchkey" placeholder="Search Stock Images by Keywords">
      <button type="submit" class="btn btn-primary"><a href="layouts/frontend/pages/search.php"> Search </a></button>
    </form>
  
  </div>
</section>

<!---------------------
----Landing-Section---
---------------------->
<div class="container">

<main class="text-center py-5">

   <div class="container">
		<div class="row text-center">
	        <div class="col">
		        <div class="counter">
				     <p class="count-text ">Total Images</p>
             <i class="fa fa-picture-o" aria-hidden="true"></i>
				      <h2 class="timer count-title count-number" data-to="20000" data-speed="1500"></h2> 
	    		</div>
	        </div>
          	<div class="col">
        	   <div class="counter">
			      <p class="count-text ">Our Users</p>
            <i class="fa fa-users" aria-hidden="true"></i>
			      <h2 class="timer count-title count-number" data-to="1700" data-speed="1500"></h2>
			   </div>
          	</div>
          	<div class="col">
           	   <div class="counter">
			      <p class="count-text ">Categories</p>
            <i class="fa fa-bars" aria-hidden="true"></i>
			      <h2 class="timer count-title count-number" data-to="11900" data-speed="1500"></h2>
			      
			   </div>
			</div>
         </div>
     </div>
</main>
<!-- <div class="discover container-fluid">
	<h2>Discover</h2>
	<section class="feature-destination">
      <div class="container">
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('assets/mats/baby.jpg')">
              <div class="text">
                <h2>Baby</h2>
                <p>Click to View</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

	 <div class="row">
	<div class="col-md-3 col-sm-3 col-xs-6"> <a href="#" class="btn btn-sm animated-button victoria-two">Explore More</a> </div>
	</div>
</div>   -->

<!-- categories Section -->
<div class="discover container">
  <h1>Over Thousands of Photos </h1><hr>
  <p>You can use our stock content for publications, TV commercials, goods packaging or smartphone applications. Because it's Royalty Free, once you purchase it you can use it for anything you need.</P>
<section class="category">
<div class="row gap-20 mb-70">
						
							<div class="col-xss-12 col-xs-6 col-sm-6">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/1.jpg') }}');">
									<a href="/image/search?searchkey=Food">
										<span class="text">
											Food
										</span>
									</a>
								</div>
							</div>
							
							<div class="col-xss-12 col-xs-6 col-sm-3">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/3.jpg') }}');">
									<a href="/image/search?searchkey=Car">
										<span class="text">
											Car
										</span>
									</a>
								</div>
							</div>
							
							<div class="col-xss-12 col-xs-6 col-sm-3">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/2.jpg') }}');">
									<a href="/image/search?searchkey=Street">
										<span class="text">
											Street
										</span>
									</a>
								</div>
							</div>

							<div class="col-xss-12 col-xs-6 col-sm-3">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/4.jpg') }}');">
									<a href="/image/search?searchkey=Building">
										<span class="text">
											Building
										</span>
									</a>
								</div>
							</div>
							
							<div class="col-xss-12 col-xs-6 col-sm-3">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/nature.jpg') }}');">
									<a href="/image/search?searchkey=Nature">
										<span class="text">
											Nature
										</span>
									</a>
								</div>
							</div>
							
							<div class="col-xss-12 col-xs-6 col-sm-6">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/6.jpg') }}');">
									<a href="/image/search?searchkey=Architecture">
										<span class="text">
											Architecture
										</span>
									</a>
								</div>
							</div>
						
                        </div>
                        </section>
                        
	<div class="text-center">
	<a href="/discover" class="btn btn-sm animated-button victoria-two d-inline-block">Explore More</a>
	</div>
</div>
                        <!-- categories section ends -->
<div class="container-fluid"> 
<div class="row text-center">
	        <div class="col">
	        <div class="foot">
          <i class="fa fa-download" aria-hidden="true"></i>
          <p class="foot-text ">Download</p>
          <p>Get beautiful stock photographs on Snapile which are free to use for both personal and business ventures. Discover inspiration in the new photos we hand-select for this library by using our search to find and download precisely what you're searching for.</p>
   			 </div>
	        </div>

          <div class="col">
           <div class="foot">
           <i class="fa fa-upload" aria-hidden="true"></i>
            <p class="foot-text ">Upload</p>
            <p>We’re looking for high-resolution, authentic images that can be used by designers, developers, bloggers, and entrepreneurs. Our users are always eager for more pictures of the culture and heritage of Nepal. If you think it’s a compelling photograph, you can upload it by signing up.</p>
          </div>
          </div>

          <div class="col">
          <div class="foot">
            <i class="fa fa-inr" aria-hidden="true"></i>
            <p class="foot-text ">Buy/Sell</p>
            <p>The photos on Snapile are royalty-free and they can be used anywhere you like once you have purchased them. The users can upload photographs for sale which are sent for approval and others can buy these photos once they are approved by us.</p>
      
		    </div>
			</div>
         </div>
    </div>
	<div class="footstart">
     	<div class="row container-fluid">
	         <h3>Get Started! Discover a wide range of images that tells a story.<br>
				Help us share this story with the world.</h3>
		</div>
	</div>
<div class="sign">
   <div class="row">
    <div class="col-md-12"> <a href="layouts/frontend/pages/register.php" class="btn btn-sm animated-button thar-two d-inline-block">Register Now</a> </div>
</div>
</div>
</div>
 <!--footer Starts-->
 <!-- Footer -->


</body>

</html>

@endsection