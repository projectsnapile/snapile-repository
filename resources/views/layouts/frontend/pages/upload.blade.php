
@extends('layouts.frontend.welcome')

<body>
@section('main-content')
  <section class="upload-photo bg-img bg-para color-white" style="background: url('{{ asset('frontend/img/bg-img/1.jpg') }}';">

  <div class="container">
 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                @endif

                @if(Session()->has('message'))
                <p class="alert alert-success">
                {{ Session()->get('message') }}
                </p>
                @endif
<form id="contact-form" method="post" enctype="multipart/form-data" action="{{ url('admin/dashboard/imageupload') }}" role="form">
  {{ csrf_field() }}

<div class="upload-form">
  <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="p_title">Title </label>
                                        <input id="p_title" type="text" name="p_title" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="p_desc">Description </label>
                                        <textarea id="p_desc" type="text" name="p_desc" class="form-control" rows="5" cols="40"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="tags"  id="demo">
                                 <div class="form-group" id="tagForm">
                                    <label>Tags </label>
                               
                                                                                                
                            <select multiple="true" name="tag[]" id="choosetag" class="select2 form-control" style="height: 36px; width: 100%;">

                             @foreach($tags as $t)
                             <option value="{{$t->name}}">{{ $t->name }}</option>
                                    @endforeach
    
                            </select>
                                    </div>
                                </div>
                            </div>
                            
                            
                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="category_id">Category </label>
                                        <select id="category_id" name="category_id" class="form-control">
                                            @foreach($categories as $c)
                                                <option value="{{$c -> id}}">{{$c -> cat_name}}</option>
                                            @endforeach
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Upload Image</label>
                                        <input type="file" name="pic" class="form-control"  accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <input type="Submit" name="submit" value="Submit" class="btn btn-success btn-md">
                        </div>
  </div>
</form>

</div>
</section>

@endsection
@section('scripts')
<div class="chat-windows"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('backend/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    
    <script type="text/javascript" src="{{ asset('frontend/assets/vendor/select2/select2.min.js')}}"></script>

    
<script type="text/javascript">

$.ajaxSetup({
headers: {
   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
    $('#choosetag').select2({
    tags: true,
    tokenSeparators: [",", " "]
});
$(document).on('change', '#choosetag', function(e){
e.preventDefault();
var tag=$('#choosetag').val();

$.ajax({
   url: "/addtag",
   type: 'post',
   dataType: 'json',
   data:{tag:tag,'_token':'{{csrf_token()}}'},
   
   success : function(response){
       var tag=[];
// console.log(response.length)
   for(let i=0;i<response.length;i++)
   {
      response[i].id!==undefined ? tag.push(response[i].id):tag.push(response[i][0].id);
   }

   $('#demo').val(tag);
   }
});
})
// $('#tagForm').submit(function(e){
//     e.preventDefault();
//     $.ajax({
//         url: "{{ url('admin/dashboard/category') }}",
//         type: 'post',
//         dataType: 'html',
//         data: $('#tagForm').serialize(),
        
//     });
// });
</script>
    <!-- <script>

$.ajaxSetup({
headers: {
   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
//     $(document).ready(function() {
//     $('.js-example-basic-multiple').select2();
// });
$(document).ready(function() {
$('#choosetag').select2({
tags: true,
tokenSeparators: [",", " "]
});

$('#updatetag').select2({
tags: true,
tokenSeparators: [",", " "]
});


});
$(document).on('change', '#choosetag', function(e){
e.preventDefault();
var tag=$('#choosetag').val();

$.ajax({
   url: "/addtag",
   type: 'post',
   dataType: 'json',
   data:{tag:tag},
   
   success : function(response){
       var tag=[];
// console.log(response.length)
   for(let i=0;i<response.length;i++)
   {
      response[i].id!==undefined ? tag.push(response[i].id):tag.push(response[i][0].id);
   }

   $('#demo').val(tag);
   }
});
}); $(document).on('change', '#updatetag', function(e){

e.preventDefault();
var tag=$('#updatetag').val();

$.ajax({
   url: "/addtag",
   type: 'post',
   dataType: 'json',
   data:{tag:tag},
   
   success : function(response){
       var tag=[];
// console.log(response.length)
   for(let i=0;i<response.length;i++)
   {
      response[i].id!==undefined ? tag.push(response[i].id):tag.push(response[i][0].id);
   }

   $('#tagid').val(tag);
   }
});
});
function gettags()
{

var tags=document.querySelectorAll(".tags");
let t=[];
tags.forEach(function(ta) {
t.push(ta.value)

});

$('#updatetag').val(t)
}
gettags(); 
function gettagsid()
{

var tags=document.querySelectorAll(".tagid");
let t=[];
tags.forEach(function(ta) {
t.push(ta.value)

});

$('#tagid').val(t)
}
gettagsid(); -->
<!-- </script> -->
@endsection
</body>

