
@extends('layouts.frontend.welcome')
 @section('main-content')
<body>
    <div class="portfolio">
        <div class="hero">
            <img src="{{ asset('images/portfolio/cover/'.$user->cover_photo) }}">
            <!--         <image xlink:href="C:\xampp\htdocs\snapile\assets\mats\baby.jpg" width="640" height="426" filter="url(#blur)"></image>

 -->   
        <div class="hero-overlay">

          <h1>{{$user->name}}</h1>
            <h2>{{$user->fname}}  &nbsp;•&nbsp; Member since {{$user->created_at->format('Y-m-d')}}</h2>
                 <div class="portfolio-cover">
                    <img style="border:2px solid #fff;width:110px;height:110px;border-radius:90px" src="{{ asset('images/portfolio/profile/'.$user->profile_photo) }}" alt="{{$user->profile_photo}}">
                </div>
        </div>
            
        </div>
    </div>
    
<!-- Share-Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Share Now!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <ul class="footer-social">
                            <li><a href="#" class="footer-social-link"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<!-- Share Modal Ends Here -->

    <!-- tabs -->

    <div class="container-fluid">
    @if(Session()->has('message'))
                <p class="alert alert-success">
                {{ Session()->get('message') }}
                </p>
                @endif
    <ul class="nav nav-tabs d-flex justify-content-center" id="myTab" role="tablist">
  <li class="nav-all">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">All</a>
  </li>
  <li class="nav-aboutme">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">About Me</a>
  </li>
  <li class="nav-message">
    <a class="nav-link" id="message-tab" data-toggle="tab" href="#message" role="tab" aria-controls="message" aria-selected="false">Send Message</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      
           <!----------------------------
-------Gallery-Section-------
----------------------------->

<section class="gallery" id="gallery">

    <div class="row no-gutters"> <!-- Row .// -->
    @foreach($user->uploads as $uploads) 
        <div class="col-lg-3 col-sm-6 col-xs-12"> <!-- COL .// -->
            <div class="gallery-img-wrap img-hover">
                <a href="{{ route('image.download',$uploads->id) }}" ><img src="{{ asset('images/thumbnails/'.$uploads->image) }}" alt="{{$uploads->name}}"><div class="img-icon">
                                    <i class="fa fa-search"></i>
                                </div></a>
            </div>
        </div> <!-- COL .// -->
        @endforeach
    </div> <!-- Row .// -->

</section>

<!----------------------------
-------Gallery-Section-------
----------------------------->

<!-- <div class="view-btn">
<button type="button" class="btn btn-outline-secondary btn-rounded waves-effect">View More</button>
</div> -->
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="aboutme">
  <div class="card d-flex justify-content-center">
  <h3>Cameras</h3>
  <img src="{{ asset('frontend/img/mats/canon1.jpg')}}"><a>Canon Eos 750D</a><br>
  <form>
      First Name: {{$user->fname}}<br>
      Last Name: {{$user->lname}}<br>
      E-mail:{{$user->email}}<br>
      Username:{{$user->username}}

  </form>
  </div>
  </div>
</div>
<div class="tab-pane fade" id="message" role="tabpanel" aria-labelledby="message-tab">
    <div class="col-md-4 offset-md-4">
        <div class="card-body">
            <form action="/user/{{$user->id}}/sendmessage" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="text" id="example-subject" name="subject" class="form-control" placeholder="Subject">
                </div>
                <textarea name="message" rows="10" cols="50"></textarea>

                <div>
                <button type="submit" class="btn btn-success m-t-20">
                    Send</button>
                </div>
            </form>
            <!-- Action part -->
        </div>
    </div>
</div>

</div>
</div>
<!-- tab section ends -->
  

</body>

</html>
@endsection