@extends('layouts.frontend.welcome')
 @section('main-content')
<body>
    <!-- @if(isset($errors))
    {{$errors}}
    @endif -->
<!-------------------------
--------Register-Main----------
-------------------------->

<section class="register-main-wrap d-flex justify-content-center align-items-center bg-para" style="background: linear-gradient(rgba(0,0,0,0.4), rgba(0,0,0,.4)), url('{{ asset('frontend/img/bg-img/63.jpg') }}');">

    <div class="register-main" id="login"> <!-- Login-Card .// -->

        <div class="banner-wrap clearfix color-white">
            <h3 class="register-welcome float-left">Welcome Back</h3>
            <p class="register-btn-label float-right">Didn't Register Yet ? <button id="bttn-register">Register Now</button></p>
        </div>

        <div class="register-body bord-bottom">
             <!-- LOGIN .// --->
                <form method="POST" action="/login">
                {{csrf_field()}}
                    <label for="email">Email Address / Membership ID *</label>
                    <input type="email" required name="email">
                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    <label for="password">Password *</label>
                    <input type="password" class="u-margin-bottom-new" required  name="password">
                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    <div class="clearfix">
                        <!-- <button class="login-submit float-left" type="button"> -->
                            <button class="login-submit float-left fa fa-sign-in" aria-hidden="true"> Sign In</button>
                        <!-- </button> -->
                        <p class="forgot-password float-right">Forgot Password? Please <span id="bttn-login1">Click here</span></p>
                    </div>
                </form>
             <!-- LOGIN .// --->

        </div>
    </div> <!-- Login-Card .// -->


    <div class="register-main" id="register"> <!-- Register-Card .// -->

        <div class="banner-wrap clearfix color-white">
            <h3 class="register-welcome float-left">Register Yourself</h3>
            <p class="register-btn-label float-right">Already Registered ? <button id="bttn-login">Login</button></p>
        </div>

        <div class="register-body bord-bottom">

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-buyer-tab" data-toggle="tab" href="#nav-buyer" role="tab" aria-controls="nav-buyer" aria-selected="true">User</a>
                <a class="nav-item nav-link" id="nav-seller-tab" data-toggle="tab" href="#nav-seller" role="tab" aria-controls="nav-seller" aria-selected="false">Photographer</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-buyer" role="tabpanel" aria-labelledby="nav-buyer-tab"><!-- TAB .// -->

                <!-- REGISTER .// -->
                <form method="POST" action="/user/register">
    {{csrf_field()}}
                    <label for="first">First Name *</label>
                    <input type="text" name="fname" required>

                    <input type="hidden" name="usertype" value="2">
                    <input type="hidden" name="role_id" value="1">

                    <label for="last">Last Name *</label>
                    <input type="text" name="lname" required>

                    <label for="email">Email Address *</label>
                    <input type="email" name="email" required>

                    <label for="last">Username *</label>
                    <input type="text" name="username" required>

                    <label for="password">Password *</label>
                    <input type="password" name="password"lass="u-margin-bottom-new" required>

                    <label for="address1">Address 1 *</label>
                    <input type="text" name="address1" required>

                    <label for="address2">Address 2 *</label>
                    <input type="text" name="address2" required>



                    <button class="login-submit fa fa-sign-in" aria-hidden="true"> Sign Up</button>

                </form>
            <!-- REGISTER .// -->

            </div><!-- TAB .// -->

            <div class="tab-pane fade" id="nav-seller" role="tabpanel" aria-labelledby="nav-seller-tab"><!-- TAB .// -->

                <!-- REGISTER .// -->
                <form method="POST" action="/user/register">
    {{csrf_field()}}
                <label for="first">First Name *</label>
                    <input type="text" name="fname" required>

                    <input type="hidden" name="usertype" value="3">
                    <input type="hidden" name="role_id" value="1">

                    <label for="last">Last Name *</label>
                    <input type="text" name="lname" required>

                    <label for="email">Email Address *</label>
                    <input type="email" name="email" required>

                    <label for="last">Username *</label>
                    <input type="text" name="username" required>

                    <label for="password">Password *</label>
                    <input type="password" name="password"lass="u-margin-bottom-new" required>

                    <label for="address1">Address 1 *</label>
                    <input type="text" name="address1" required>

                    <label for="address2">Address 2 *</label>
                    <input type="text" name="address2" required>

                    <!-- <button class="login-submit" type="button"><i class="fas fa-sign-in-alt"></i> Sign Up</button> -->
                    <button class="login-submit fa fa-sign-in" aria-hidden="true"> Sign Up</button>

                </form>
            <!-- REGISTER .// -->

            </div><!-- TAB .// -->
        </div>

            <!-- REGISTER .// -->
                <!-- <form action="register">

                    <label for="first">First Name *</label>
                    <input type="text" required>

                    <label for="last">Last Name *</label>
                    <input type="text" required>

                    <label for="username">Contact Number *</label>
                    <input type="text" required>

                    <label for="email">Email Address *</label>
                    <input type="email" required>

                    <label for="password">Password *</label>
                    <input type="password" class="u-margin-bottom-new" required>

                    <button class="login-submit" type="button"><i class="fas fa-sign-in-alt"></i> Sign Up</button>

                </form> -->
            <!-- REGISTER .// -->

        </div>
       
    </div> <!-- Register-Card .// -->

    <div class="register-main" id="reset"> <!-- Reset-Card .// -->

        <div class="banner-wrap clearfix color-white">
            <h3 class="register-welcome float-left">Reset Password</h3>
            <p class="register-btn-label float-right">Go Back to login page <button id="bttn-login2">Login</button></p>
        </div>

        <div class="register-body bord-bottom">

            <!-- RESET .// -->
                <form action="register">

                    <label for="email">Email Address *</label>
                    <input type="email" class="u-margin-bottom-new" required>

                    <!-- <button class="login-submit" type="button"> -->
                    <a class="login-submit" href="index.php">Recover Password</a>
                    <!-- </button> -->

                </form>
            <!-- RESET .// -->

        </div>
    </div> <!-- Reset-Card .// -->

</section>

<!-------------------------
--------Register-Main----------
-------------------------->

</body>
</html>
@endsection