
@extends('layouts.frontend.welcome')
 @section('main-content')
<body>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<section class="banner bg-para" style="background-image: linear-gradient(to top right, rgba(5, 25, 34, 0.3), rgba(10, 38, 51, 0.3)), url(img/bg-img/1.jpg);">   
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->
            <div class="col-md-6"> <!-- Col .// -->
                <h3>Terms & Conditions</h3>
            </div> <!-- Col .// -->
        </div>  <!-- Row .// -->
    </div> <!-- Container .// -->
</section>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<!----------------------------
-------Terms-Main-------
----------------------------->

<section class="terms-main common-padding">
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->
            <div class="col-3"> <!-- Col .// -->
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Terms & Conditions</a>
                <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Privacy Policy</a>
                <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Payment method</a>
                <!-- <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a> -->
                </div>
            </div> <!-- Col .// -->
            <div class="col-9 "> <!-- Col .// -->
                <div class="tab-content normal-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        <h5>Terms & condition by Snapile</h5>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Optio deleniti sequi voluptatem sapiente molestiae nesciunt exercitationem consectetur, quae velit laborum expedita debitis. Porro similique neque, odit nemo perspiciatis labore aspernatur.</p>
                        <h6>Terms and Condition on Booking</h6>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, nostrum et ab unde, iure accusantium repudiandae itaque ex eligendi saepe rem quia dolores placeat natus deleniti quae ipsa veniam, veritatis nesciunt. Totam reprehenderit nesciunt nostrum temporibus molestiae impedit, quae, ducimus, ab rem eveniet incidunt obcaecati fugit suscipit deleniti vel amet.</p>

                        <h6>Terms and Condition on Payment</h6>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, nostrum et ab unde, iure accusantium repudiandae itaque ex eligendi saepe rem quia dolores placeat natus deleniti quae ipsa veniam, veritatis nesciunt. Totam reprehenderit nesciunt nostrum temporibus molestiae impedit, quae, ducimus, ab rem eveniet incidunt obcaecati fugit suscipit deleniti vel amet.</p>

                        <h6>Terms and Condition on Refund</h6>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, nostrum et ab unde, iure accusantium repudiandae itaque ex eligendi saepe rem quia dolores placeat natus deleniti quae ipsa veniam, veritatis nesciunt. Totam reprehenderit nesciunt nostrum temporibus molestiae impedit, quae, ducimus, ab rem eveniet incidunt obcaecati fugit suscipit deleniti vel amet.</p>

                    </div> 
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                        <h5>Privacy policy of Snapile</h5>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ullam facilis placeat laudantium quam distinctio veniam ipsam cumque commodi hic aperiam. Neque saepe officiis sint distinctio consequuntur, voluptatem ad amet fuga?</p>


                        <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit quo saepe ad libero incidunt, minima fugit magnam alias, reprehenderit in eveniet repellendus expedita consequatur neque quas iure quod atque necessitatibus soluta labore laboriosam. Labore laboriosam cumque veniam eos repellat doloribus consectetur inventore architecto pariatur ex, error commodi ullam magni voluptates reprehenderit consequatur? Omnis odit fuga officia maxime tempore eveniet reprehenderit quas illum. Unde sapiente excepturi, accusamus vitae repellat magni? Voluptatum aut voluptas dolor pariatur ea saepe voluptatem consequuntur ducimus fugit!</p>
                        <h6>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cumque, asperiores? Nulla ab cupiditate nemo libero esse in aspernatur eveniet id dolore fuga, et, adipisci fugiat labore natus qui accusamus doloremque.</h6>

                        <p class="term-bg p-4 font-italic">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit, quisquam temporibus quasi qui nobis totam veritatis mollitia rem similique soluta dolorem fugit aliquam illo id consectetur quas nihil, quaerat ea maxime dolores assumenda porro veniam?</p>
                        <h6>Terms and Condition on Refund</h6>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, nostrum et ab unde, iure accusantium repudiandae itaque ex eligendi saepe rem quia dolores placeat natus deleniti quae ipsa veniam, veritatis nesciunt. Totam reprehenderit nesciunt nostrum temporibus molestiae impedit, quae, ducimus, ab rem eveniet incidunt obcaecati fugit suscipit deleniti vel amet.</p>
                    </div>
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">

                    <h5>Payment of Snapile</h5>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ullam facilis placeat laudantium quam distinctio veniam ipsam cumque commodi hic aperiam. Neque saepe officiis sint distinctio consequuntur, voluptatem ad amet fuga?</p>

                    <div class="media u-margin-top-small">
                        <div class="media-body">
                        <h5 class="mt-0">Khalti</h5>
                        <p>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                        </div>
                    </div>

                    <div class="media u-margin-top-small">
                        <div class="media-body">
                        <h5 class="mt-0">E-Sewa</h5>
                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                        </div>
                    </div>
                    </div>
                </div>
            </div> <!-- Col .// -->
        </div> <!-- Row .// -->
    </div> <!-- Container .// -->
</section>



<!----------------------------
-------Terms-Main-------
----------------------------->


</body>
</html>
@endsection