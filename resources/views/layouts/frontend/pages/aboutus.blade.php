
@extends('layouts.frontend.welcome')
 @section('main-content')
<body>
<!-- About Us Section -->
<div class="aboutus">
    <div class="aboutus-title">
        <h3>About us </h3>
        <hr>
        <p class="aboutus-intro">Snapile is a website that focuses on a “stock photo library” concept. The main aim of the website is to provide endless supply of stock photos for the users. This is a website accessible by anyone who has connection to the internet.</p>
        <p class="aboutus-intro">The name itself holds a significant meaning, Snapile, meaning a pile of snaps. It will incorporate all the services required by the content creators as well as the photographers who are providing their hard obtained photographs for the content creators to use. </p>
        <p class="aboutus-intro">The main target of the website are the content creators and graphic designers who are in constant need of the stock photos. It will incorporate all the services required by the content creators as well as the photographers who are providing their hard obtained photographs for the content creators to use.</p>
        <p class="aboutus-intro">This website does not only focus on the content creators getting the images they require but also to provide the photographers a chance to not only showcase their skills but also to gain an employment opportunity through the website.</p>
    </div>
    <div class="whyus">
        <h3>Why us</h3>
        <hr>
        <p>The most common problem faced by the Nepalese content creators are the lack of relevant images they can find in the stock photo library. Snapile is designed to solve the problem of the Nepalese content creators and graphic designers by helping them find relevant Nepalese content in a hassle-free way.</p>
    </div>
    <div class="whatsthere">
        <h3>What’s there for Photographers?</h3>
        <hr>
        <p>A major objective of Snapile is to provide the photographers a chance to not only showcase their skills but also to gain an employment opportunity through the website. The website can give photographers the exposure they need.</p>
    </div>
    <div class="aboutus-category">
        <h3>Categories</h3>
        <hr>
        <p class="category-intro">Explore a wide range of images belonging to variety of categories on Snapile.</p>
        <p class="category-intro">It's so much easier to search and find what you are looking for here.
        
    </div>

</div>



</body>
</html>
@endsection