
@extends('layouts.frontend.welcome')
 @section('main-content')
<body>

<!---------------------------------
---------BLog Banner--------------
---------------------------------->

<section class="hero blog-banner">
    <div class="hero-image-wrapper" style="background: linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url('{{ asset('frontend/img/bg-img/3.jpg') }}');">
    </div>
    <div class="hero-content-wrapper">
        <p class="hero-normal">Blog Page</p>
    </div>
</section>


<!---------------------------------
---------BLog Banner--------------
---------------------------------->

<!---------------------------------
---------BLog Main--------------
---------------------------------->

<section class="blog-main bg-white">
    <div class="container"> <!-- Container .// --->

        <div class="row"> <!-- Row .// --->

            <div class="col-lg-9"> <!-- COL .// --->
                <div class="row"> <!-- Inner-Row .// --->
                    @foreach($blogs as $b)
                    <div class="col-lg-6 col-sm-12"> <!-- Inner-Col .// --->

                        <div class="blog-card"> <!-- Card.// -->
                            <a href="{{ url('blog-detail/' . $b -> id)}}">
                                <div class="blog-cover">
                                    <img src="{{asset('images/blogs/'.$b -> image) }}" alt="{{ $b -> b_title}}">
                                </div>
                            </a>
                            <div class="blog-content-wrapper">
                                <p class="blog-date text-center">{{$b -> created_at->format('Y-m-d') }}</p>
                                <h3 class="blog-title text-center"><a href="{{ url('blog-detail/' .$b -> id)}}" >{{ $b -> b_title}}</a></h3>
                                <p class="about-para">{!! str_limit($b -> b_detail, $limit = 300, $send = '...') !!}</p>
                            </div>
                        </div> <!-- Card.// -->

                    </div> <!-- Inner-Col .// --->
                    @endforeach
                </div> <!-- Inner-Row .// --->

            </div> <!-- COL .// --->

            <div class="col-lg-3"> <!-- COL .// --->
                <div class="blog-side-card u-margin-bottom-medium"> <!-- Side-Card.// -->
                    <h4 class="blog-side-title">Popular Posts</h4>
                    @foreach($blogs as $blo)
                    <div class="blog-side-post-wrap">
                        <a href="{{ url('blog-detail/' .$blo -> id) }}" class="blog-side-post-title">{{ $blo -> b_title}}</a>
                        <p class="blog-date">{{$blo -> created_at->format('Y-m-d') }} </p>
                    </div>
                    @endforeach

                </div> <!-- Side-Card.// -->
                <!-- Side-Card.// -->
                <!-- <div class="blog-side-card"> 
                    <h4 class="blog-side-title">Instagram</h4>

                    <div class="blog-side-post-wrap">
                        <div class="row less-gutter">
                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/1.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/2.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/3.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/4.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/5.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/6.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                </div>  --><!-- Side-Card.// -->

            </div> <!-- COL .// --->

        </div> <!-- Row .// --->

    </div> <!-- Container .// --->
</section>

<!---------------------------------
---------BLog Main--------------
---------------------------------->


</body>
</html>
@endsection