
@extends('layouts.frontend.welcome')
 @section('main-content')

<body>
    <!---------------------------------
---------BLog Banner--------------
---------------------------------->

<section class="blog-detail-cover">
    <img src="{{ asset('images/blogs/'.$blog -> image) }}" alt="{{ $blog -> b_title}}" class="img-cover">
</section>

<!---------------------------------
---------BLog Banner--------------
---------------------------------->

<!---------------------------------
---------BLog-Detail-main--------------
---------------------------------->

<section class="blog-detail-main common-padding bg-white">
    <div class="container">
        <div class="blog-detail-heading-wrap">
            <p class="blog-date text-center">{{$blog -> created_at->format('Y-m-d') }}</p>
            <h3 class="blog-title text-center">{{ $blog -> b_title}}</h3>
        </div>

        <div class="row"> <!-- ROW .// --->

            <div class="col-lg-9"> <!-- COL .// --->

               {!! $blog -> b_detail!!}

                <div class="blog-author-wrapper"><!-- 
                    <div class="blog-author-heading clearfix">
                        <p class="blog-detail-author-description float-left"><i class="fa fa-tags"></i><a href="#"> Nature,</a><a href="#"> Street,</a><a href="#"> Lights,</a><a href="#"> Portraits</a></p>
                        <p class="blog-detail-author-share float-right"><a href="#"><i class="fa fa-share-alt"></i> Share</a></p>
                    </div> -->
                    <div class="blog-author-body">
                        <div class="author-photo">
                            <img src="{{ asset('images/portfolio/profile').'/'.$blog->user->profile_photo }}" alt="{{$blog->user->profile_photo}}" class="img-cover">
                        </div>
                        <p class="author-title text-center">Author</p>
                        <p class="author-position text-center">{{$blog->user->name}}</p>
                    </div>
                </div>

            </div> <!-- COL .// --->

        <!-- Side-Card.// -->
         <div class="col-lg-3"> <!-- COL .// --->
                <div class="blog-side-card u-margin-bottom-medium"> <!-- Side-Card.// -->
                    <h4 class="blog-side-title">Popular Posts</h4>
                    @foreach($blogs as $blo)
                    <div class="blog-side-post-wrap">
                        <a href="{{ url('blog-detail/' .$blog -> id) }}" class="blog-side-post-title">{{ $blo -> b_title}}</a>
                        <p class="blog-date">{{$blo -> created_at->format('Y-m-d') }} </p>
                    </div>
                    @endforeach

                </div> <!-- Side-Card.// -->
                <!-- Side-Card.// -->
                <!-- <div class="blog-side-card"> 
                    <h4 class="blog-side-title">Instagram</h4>

                    <div class="blog-side-post-wrap">
                        <div class="row less-gutter">
                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/1.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/2.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/3.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/4.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/5.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="side-image-wrap">
                                    <a href="#">
                                        <img src="img/bg-img/6.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                </div>  --><!-- Side-Card.// -->

            </div>

            </div> <!-- COL .// --->

        </div> <!-- ROW .// --->
    </div>
</section>

<!---------------------------------
---------BLog-Detail-main--------------
---------------------------------->

</body>
</html>
@endsection 