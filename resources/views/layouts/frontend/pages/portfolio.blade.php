
@extends('layouts.frontend.welcome')
 @section('main-content')
<body>
    <div class="portfolio">
        <div class="hero">
            <img src="{{ asset('images/portfolio/cover').'/'.Auth()->user()->cover_photo }}">
            <!--         <image xlink:href="C:\xampp\htdocs\snapile\assets\mats\baby.jpg" width="640" height="426" filter="url(#blur)"></image>

 -->   
        <div class="hero-overlay" style="position:absolute;">

          <h1>{{Auth::user()->name}}</h1>
            <h2>{{Auth::user()->fname}}  &nbsp;•&nbsp; Member since {{Auth::user()->created_at->format('Y-m-d')}}</h2>
                 <div class="portfolio-cover">
                    <img style="border:2px solid #fff;width:110px;height:110px;border-radius:90px" src="{{ asset('images/portfolio/profile').'/'.Auth()->user()->profile_photo }}" alt="{{Auth()->user()->profile_photo}}">
                </div>
                <div class="share-btn" >
                <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModalCenter">Share</button>
                <button type="button" class="btn btn-dark float-left" ><a href="{{ url('/upload') }}">Upload</a></button>
                </div>
        </div>
            
        </div>
    </div>
    
<!-- Share-Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Share Now!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <ul class="footer-social">
                            <li><a href="#" class="footer-social-link"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<!-- Share Modal Ends Here -->

    <!-- tabs -->

    <div class="container-fluid">
    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                @endif

                @if(Session()->has('message'))
                <p class="alert alert-success">
                {{ Session()->get('message') }}
                </p>
                @endif
    <ul class="nav nav-tabs d-flex justify-content-center" id="myTab" role="tablist">
  <li class="nav-all">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">All</a>
  </li>
  <li class="nav-aboutme">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">About Me</a>
  </li>
  @if(Auth()->check())
  <li class="nav-blog">
    <a class="nav-link" id="blog-tab" data-toggle="tab" href="#blog" role="tab" aria-controls="blog" aria-selected="false">Create Blog</a>
  </li>
  <li class="nav-message">
    <a class="nav-link" id="message-tab" data-toggle="tab" href="#message" role="tab" aria-controls="message" aria-selected="false">Messages<span style="color:white; background:red; border:1px solid red; border-radius:50%; padding-left: 5px;
    padding-right: 5px;"> {{count(Auth()->user()->messagecount)}}</span></a>
  </li>
  @endif
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      
           <!----------------------------
-------Gallery-Section-------
----------------------------->

<section class="gallery" id="gallery">

    <div class="row no-gutters"> <!-- Row .// -->
    @foreach(Auth::user()->uploads as $uploads)  
        <div class="col-lg-3 col-sm-6 col-xs-12"> <!-- COL .// -->
            <div class="gallery-img-wrap img-hover">
                <a href="{{ route('image.download',$uploads->id) }}"><img src="{{ asset('images/thumbnails/'.$uploads->image) }}" alt="{{$uploads->name}}"><div class="img-icon">
                                    <i class="fa fa-search"></i>
                                </div></a>
            </div>
        </div> <!-- COL .// -->
        @endforeach
    </div> <!-- Row .// -->

</section>

<!----------------------------
-------Gallery-Section-------
----------------------------->

<!-- <div class="view-btn">
<button type="button" class="btn btn-outline-secondary btn-rounded waves-effect">View More</button>
</div> -->
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="aboutme">
  <div class="card d-flex justify-content-center">
  <h3>Cameras</h3>
  <img src="{{ asset('frontend/img/mats/canon1.jpg')}}"><a>Canon Eos 750D</a><br>
  <form>
      First Name: {{Auth::user()->fname}}<br>
      Last Name: {{Auth::user()->lname}}<br>
      E-mail:{{Auth::user()->email}}<br>
      Username:{{Auth::user()->username}}

  </form>
  </div>
  </div>
  @if(Auth()->check())
      <form id="contact-form" method="post" enctype="multipart/form-data" action="{{ url('/portfolio/'.Auth()->user()->id) }}" role="form">
                    {{ csrf_field() }}
                    <div class="messages"></div>
                    <div class="controls">        
                        <div class="row">
                        <div class="col-md-4 offset-md-4">
                                <div class="form-group">
                                    <label>Upload Profile Picture</label>
                                    <input type="file" name="profile_photo" class="form-control"  accept="image/*">
                                </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-4 offset-md-4">
                                <div class="form-group">
                                    <label>Upload Cover Picture</label>
                                    <input type="file" name="cover_photo" class="form-control"  accept="image/*">
                                </div>
                        </div>
                        </div>
                        <div class="col-md-4 offset-md-4">
                        <input type="Submit" name="submit" value="Submit" class="btn btn-success btn-md" style="margin-left: 10.5rem;">
                        </div>
                    </div><br><br>
</form>
            @endif
</div>
@if(Auth()->check())
<div class="tab-pane fade" id="blog" role="tabpanel" aria-labelledby="blog-tab">
  <div class="createblog">
    <h2>Create Blog</h2>
    <hr>
    <form id="contact-form" method="post" enctype="multipart/form-data" action="{{ url('/portfolio') }}" role="form">
    {{ csrf_field() }}
    <div class="form-group">
    <label for="exampleInputtitle">Title</label>
    <input class="form-control" id="b_title" type="text" name="b_title" placeholder="Enter your title">
  </div>
  <div class="form-group">

  <input id="b_author" type="hidden" name="b_author" value="{{Auth::user()->id}}" class="form-control">
  </div>
  <div class="form-group">
    <label for="exampleInputDescription">Description</label>
    <!-- <input type="Description" class="form-control" id="exampleInputdescription1" aria-describedby="descriptionhelp" placeholder="Enter your Description">  -->
  <textarea id="b_detail" type="text" name="b_detail" class="form-control" placeholder="Enter your Description" cols="90" rows="5"></textarea>
  </div>
  <div class="form-group">
      <label>Upload Image</label>
      <input type="file" name="blogpic" class="form-control"  accept="image/*">
  </div>
  <input type="Submit" name="submit" value="Submit" class="btn btn-primary btn-md">
</form>
</div>
</div>
@endif

<div class="tab-pane fade" id="message" role="tabpanel" aria-labelledby="message-tab">
  <br>
  <div class="row">    
  @foreach(Auth::user()->receiver as $receiver)              
      <div class="col-md-6">
        <div class="card border-dark">
          <div class="card-header bg-dark">
                  <h4 class="m-b-0 text-white">From:<a href="/user/{{$receiver->sender->id}}/viewprofile" style="color:white;"> {{$receiver->sender->name}}</a></h4></div>
              <div class="card-body">
                <form method="post" action="/changeusermessagestatus/{{$receiver->id}}">
                  {{csrf_field()}}
                  <input type='submit' class="btn {{$receiver->status==1?'btn-success':'btn-danger'}} btn-sm" value=" {{$receiver->status==1?'Seen':'Unseen'}}">

                </form><br>
                  <h6 class="card-title">Email: {{$receiver->sender->email}}</h6>
                  <h6 class="card-title">Subject: {{$receiver->subject}}</h6>
                  <p class="card-text">{{$receiver->message}}</p>
              </div>
          </div>
      </div> 
      @endforeach 
  </div>
</div>
</div>
</div><br><br>
<!-- tab section ends -->
  

</body>

</html>
@endsection