
@extends('layouts.frontend.welcome')
 @section('main-content')
<body>
     <!----------------------------
-------Breadcrumb-------
----------------------------->

<section class="banner bg-para" style="background-image: linear-gradient(to top, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.3)), url('{{asset('frontend/img/bg-img/18.jpg')}}');">   
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->
            <div> <!-- Col .// -->
                <h3>Discover Image</h3>
            </div> <!-- Col .// -->
        </div>  <!-- Row .// -->

        <div class="u-margin-top-small">
            <div class="row"> <!-- Row .// -->

                <div class="col-lg-8 offset-lg-2"> <!-- Col .// -->

                    <div class="uni-title">
                        <h5 class="color-white text-center"><i class="fa fa-search" aria-hidden="true"></i> Search Images</h5>
                    </div>

                    <form action="search" method="GET" action="/image/search">
                        <div class="row"> <!-- Inner Row .// -->
                            <div class="col-md-10"> <!-- Inner Col .// -->
                                <div class="input-area clearfix">
                                    <div class="input-group-prepend float-left">
                                        <span class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></span>
                                    </div>
                                    <input type="text" name="searchkey" placeholder="Want a HQ Picture?" required>
                                </div>
                            </div> <!-- Inner Col .// -->
                            <div class="col-md-2"> <!-- Inner Col .// -->

                                <div class="input-area">
                                    <button>&rarr;</button>
                                </div>
                            
                            </div> <!-- Inner Col .// -->
                        </div> <!-- Inner Row .// -->
                    </form>
                
                </div> <!-- Col .// -->
            </div> <!-- Row .// -->
        </div>

    </div> <!-- Container .// -->
</section>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<div class="downloadpage container">

              
       
    <div class="row">
    <div class="col-md-6 image">
        <img src="{{ asset('images/watermark/'.$uploads -> image) }}" alt="image" srcset="">
    </div>
    <div class="col-md-6 downloadoption">
        <img src="{{asset('/images/portfolio/profile/'.$uploads->user->profile_photo)}}" alt="{{$uploads->p_title}}" srcset=""><h6>By<a href="/user/{{$uploads->user->id}}/viewprofile"> {{$uploads->user->name}}</a></h6><hr>
        <p>{{$uploads->p_desc}}</p><hr>
        <div class="exifdata">
            <h4>Exif:</h4>
            <?php 
              $exif = json_decode($uploads->p_exif, true);
              
              echo "Resolution: " . $exif['Resolution']; echo "<br>";
              echo "Model: " . $exif['Model']; echo "<br>";
              echo "Snap Info: " . $exif['SnapInfo']; echo "<br>";
            ?>
        </div><hr>
<!--     <button type="button" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i> Free Download</button>
 -->
    <button class="btn btn-primary" id="payment-button" data-image="{{$uploads}}">Buy (Khalti)</button> 
    </div>
    </div>
    <div class="downloadpage-tags clearfix">
            <p class="downloadpage-description float-left"><i class="fa fa-tags"></i>
        @foreach($uploads->tags as $tag)
        <a href="/image/search?searchkey={{$tag->name}}">{{$tag->name}}</a> 
        @endforeach
        </p><p class="downloadpage-share float-right"><a href="#" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-share" aria-hidden="true"></i> Share</a></p>
    </div>
                   
<!-- Share-Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Share Now!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <ul class="footer-social">
                            <li><a href="#" class="footer-social-link"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-social-link"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<!-- Share Modal Ends Here -->
<div class="downloadcategory">
<h1>Explore More </h1>
                    <section class="category">
<div class="row gap-20 mb-70">
						
							<div class="col-xss-12 col-xs-6 col-sm-6">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/1.jpg') }}');">
									<a href="/image/search?searchkey=Food">
										<span class="text">
											Food
										</span>
									</a>
								</div>
							</div>
							
							<div class="col-xss-12 col-xs-6 col-sm-3">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/3.jpg') }}');">
									<a href="/image/search?searchkey=Car">
										<span class="text">
											Car
										</span>
									</a>
								</div>
							</div>
							
							<div class="col-xss-12 col-xs-6 col-sm-3">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/2.jpg') }}');">
									<a href="/image/search?searchkey=Street">
										<span class="text">
											Street
										</span>
									</a>
								</div>
							</div>

							<div class="col-xss-12 col-xs-6 col-sm-3">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/4.jpg') }}');">
									<a href="/image/search?searchkey=Building">
										<span class="text">
											Building
										</span>
									</a>
								</div>
							</div>
							
							<div class="col-xss-12 col-xs-6 col-sm-3">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/5.jpg') }}');">
									<a href="/image/search?searchkey=Nature">
										<span class="text">
											Nature
										</span>
									</a>
								</div>
							</div>
							
							<div class="col-xss-12 col-xs-6 col-sm-6">
								<div class="category-image-bg" style="background-image:url('{{ asset('frontend/img/bg-img/6.jpg') }}');">
									<a href="/image/search?searchkey=Architecture">
										<span class="text">
											Architecture
										</span>
									</a>
								</div>
							</div>
						
                        </div>
                        </section>
                        </div>
</div>
</body>
</html>

@endsection


@section ('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
    var Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
})


        $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        var config = {
            // replace the publicKey with yours
            "publicKey": "test_public_key_a2b2a99a3a3c4b1882fbbf49e0c2f594",
            "productIdentity": "{{$uploads->id}}",
            "productName": "{{$uploads->p_title}}",
            "productUrl": "/image/{{$uploads->id}}/download",
            "eventHandler": {
                onSuccess (payload) {
                    // hit merchant api for initiating verfication
                    console.log(payload);
                    $.ajax({
                    url:"/payment/verification",
                    type: 'get',
                    data:{
                    amount : payload.amount,
                    trans_token : payload.token
                    },
                    success: function(res)
                    {
                     
                        Toast.fire({
                    type: 'success',
                    title: 'Transaction Successful'
                    })
                    var a = document.createElement('a');
a.href = "/images/{{$uploads->image}}";
a.download = "{{$uploads->image}}";
document.body.appendChild(a);
a.click();
document.body.removeChild(a);
                    },
                    error: function(error)
                    {
                        Toast.fire({
                    type: 'error',
                    title: 'Transaction Failed'
                    })
                    }
                    })
                    },
                onError (error) {
                    console.log(error);
                },
                onClose () {
                    console.log('widget is closing');
                }
            }
        };

        var checkout = new KhaltiCheckout(config);
        var btn = document.getElementById("payment-button");
        btn.onclick = function () {
            checkout.show({amount: 50000});
        }
    </script>

    @endsection