@extends('layouts.frontend.welcome')
 @section('main-content')

  <body>
    <section class="page-form">
    <div class="container">

            <div class="col-md-7 col-sm-7 form-area" data-aos="zoom-in">


                <h4>Blog Upload Test</h4>
                 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                @endif

                @if(Session()->has('message'))
                <p class="alert alert-success">
                {{ Session()->get('message') }}
                </p>
                @endif

                <form id="contact-form" method="post" enctype="multipart/form-data" action="{{ url('admin/dashboard/blog') }}" role="form">
                    {{ csrf_field() }}
                    <div class="messages"></div>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="b_title">Title </label>
                                    <input id="b_title" type="text" name="b_title" class="form-control">
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="b_author">Author </label>
                                    <input id="b_author" type="text" name="b_author" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="b_desc">Description </label>
                                    <textarea id="b_detail" type="text" name="b_detail" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                       <!--  <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="b_desc">Tag </label>
                                    <input id="tag" type="text" name="tag" class="form-control">
                                </div>
                            </div>
                        </div> -->
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Image</label>
                                    <input type="file" name="blogpic" class="form-control"  accept="image/*">
                                </div>
                            </div>
                        </div>
                        <input type="Submit" name="submit" value="Submit" class="btn btn-success btn-md">
                    </div>

                </form>

            </div>
        </div>
    </div>
</section>

  </body>
</html>
@endsection