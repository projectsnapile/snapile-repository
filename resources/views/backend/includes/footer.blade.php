  <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Snapile 2019. Designed and Developed by
                <a href="">Snapile</a>.
            </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->

    <div class="chat-windows"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('backend/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('backend/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{ asset('backend/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- apps -->
    <script src="{{ asset('backend/dist/js/app.min.js')}}"></script>
    <script src="{{ asset('backend/dist/js/app.init.js')}}"></script>
    <script src="{{ asset('backend/dist/js/app-style-switcher.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('backend/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{ asset('backend/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('backend/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('backend/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('backend/dist/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="{{ asset('backend/assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{ asset('backend/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <!--c3 charts -->
    <script src="{{ asset('backend/assets/extra-libs/c3/d3.min.js')}}"></script>
    <script src="{{ asset('backend/assets/extra-libs/c3/c3.min.js')}}"></script>
    <!--chartjs -->
    <script src="{{ asset('backend/assets/libs/raphael/raphael.min.js')}}"></script>
    <script src="{{ asset('backend/assets/libs/morris.js/morris.min.js')}}"></script>

    <script src="{{ asset('backend/dist/js/pages/dashboards/dashboard1.js')}}"></script>
    
    <script src="{{ asset('backend/dist/js/select2.min.js')}}"></script>
        <script src="{{ asset('backend/assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('backend/assets/libs/magnific-popup/meg.init.js')}}"></script>
      <script src="{{asset('backend/assets/libs/tinymce/tinymce.min.js')}}"></script>
   
    <script>

     $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//     $(document).ready(function() {
//     $('.js-example-basic-multiple').select2();
// });
 $(document).ready(function() {
$('#choosetag').select2({
    tags: true,
    tokenSeparators: [",", " "]
});

$('#updatetag').select2({
    tags: true,
    tokenSeparators: [",", " "]
});


});
 $(document).on('change', '#choosetag', function(e){

  var tag=$('#choosetag').val();

    $.ajax({
        url: "/addtag",
        type: 'post',
        dataType: 'json',
        data:{tag:tag},
        
        success : function(response){
            var tag=[];
    // console.log(response.length)
        for(let i=0;i<response.length;i++)
        {
           response[i].id!==undefined ? tag.push(response[i].id):tag.push(response[i][0].id);
        }

        $('#demo').val(tag);
        }
    });
}); $(document).on('change', '#updatetag', function(e){

  var tag=$('#updatetag').val();

    $.ajax({
        url: "/addtag",
        type: 'post',
        dataType: 'json',
        data:{tag:tag},
        
        success : function(response){
            var tag=[];
    // console.log(response.length)
        for(let i=0;i<response.length;i++)
        {
           response[i].id!==undefined ? tag.push(response[i].id):tag.push(response[i][0].id);
        }

        $('#tagid').val(tag);
        }
    });
});
   function gettags()
{

  var tags=document.querySelectorAll(".tags");
  let t=[];
tags.forEach(function(ta) {
    t.push(ta.value)

});

$('#updatetag').val(t)
}
gettags(); 
  function gettagsid()
{

  var tags=document.querySelectorAll(".tagid");
  let t=[];
tags.forEach(function(ta) {
    t.push(ta.value)

});

$('#tagid').val(t)
}
gettagsid();

function changestatus(id)
{
   
    $.ajax({
        url: "/changestatus",
        type: 'post',
        dataType: 'text',
        data:{id:id},
        
        success : function(response){
          $( "#status"+id ).load(window.location.href + " #status"+id );
    
    }
    });
}

function blogstatus(id)
{
   
    $.ajax({
        url: "/blogstatus",
        type: 'post',
        dataType: 'text',
        data:{id:id},
        
        success : function(response){
          $( "#b_status"+id ).load(window.location.href + " #b_status"+id );
    
    }
    });
}

$(document).ready(function() {

        if ($("#mymce").length > 0) {
            tinymce.init({
                selector: "textarea#mymce",
                theme: "modern",
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

            });

        }
    });
    $('#blogedit-form').bind('form-pre-serialize', function(e) {
    tinymce.triggerSave();
    });


</script>
<script>
        var config = {
            // replace the publicKey with yours
            "publicKey": "test_public_key_aec63981d5bd47a59f6d126ce6d5e320",
            "productIdentity": "1234567890",
            "productName": "Dragon",
            "productUrl": "http://gameofthrones.wikia.com/wiki/Dragons",
            "eventHandler": {
                onSuccess (payload) {
                    // hit merchant api for initiating verfication
                    console.log(payload);
                },
                onError (error) {
                    console.log(error);
                },
                onClose () {
                    console.log('widget is closing');
                }
            }
        };

        var checkout = new KhaltiCheckout(config);
        var btn = document.getElementById("payment-button");
        btn.onclick = function () {
            checkout.show({amount: 1000});
        }
    </script>