  <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                      
                        <li>
                            <h4 class="hide-menu" style="color:white; margin:0px 25px;">Stock Photo Library</h4>
                        </li><br><br>
                        <li class="sidebar-item">
                            <a href="{{ url('admin/dashboard') }}" class="sidebar-link waves-effect waves-dark" aria-expanded="false">
                                <i class="icon-Car-Wheel"></i>
                                <span class="hide-menu">Dashboard </span>
                            </a>
                        </li>
                        <li class="nav-small-cap">
                            <i class="mdi mdi-dots-horizontal"></i>
                            <span class="hide-menu">Categories CRUD</span>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link  waves-effect waves-dark " href="{{ url('admin/dashboard/category') }}" >
                                <i class="mdi mdi-notification-clear-all"></i>
                                <span class="hide-menu">Category </span>
                            </a>
                            <!-- <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a href="{{ url('admin/dashboard/category/create') }}" class="sidebar-link">
                                        <span class="hide-menu"> Create New </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="{{ url('admin/dashboard/category') }}" class="sidebar-link">
                                        <span class="hide-menu"> View</span>
                                    </a>
                                </li>
                            </ul> -->
                        </li>
                        <li class="nav-small-cap">
                            <i class="mdi mdi-dots-horizontal"></i>
                            <span class="hide-menu">Approve/Disapprove Images</span>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{ url('admin/dashboard/imageupload') }}" class="sidebar-link waves-effect waves-dark" aria-expanded="false">
                                <i class="icon-Files"></i>
                                <span class="hide-menu">Image Requests </span>
                            </a>
                        </li>
                        <li class="nav-small-cap">
                            <i class="mdi mdi-dots-horizontal"></i>
                            <span class="hide-menu">Approve/Disapprove Blogs</span>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{ url('admin/dashboard/blog') }}" class="sidebar-link waves-effect waves-dark" aria-expanded="false">
                                <i class="icon-Receipt-4"></i>
                                <span class="hide-menu">Blog </span>
                            </a>
                        </li>
                        <li class="nav-small-cap">
                            <i class="mdi mdi-dots-horizontal"></i>
                            <span class="hide-menu">Logout from Dashboard</span>
                        </li>
                        <li class="sidebar-item">

                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('logout') }}" aria-expanded="false" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="mdi mdi-directions"></i>
                                <span class="hide-menu">Log Out</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->