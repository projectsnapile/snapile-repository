<?php

namespace Snapile;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable=['sender_id','receiver_id','subject','message','status'];

    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }
    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }
    //
}
