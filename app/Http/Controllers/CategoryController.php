<?php

namespace Snapile\Http\Controllers;

use Illuminate\Http\Request;
use Snapile\Http\Controllers\Controller;
use Snapile\Models\Category;
use Snapile\Models\Tag;
use Form;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::paginate(10);
        return view('layouts.backend.pages.category.category',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags=Tag::all();
        return view('layouts.backend.pages.category.addcategory',compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

 
         $this->validate($request, [
            'cat_name' => 'required',
    
        ]);

        Session::flash('message', 'Success');

        $catinfo = new Category();
        $catinfo -> cat_name = $request -> cat_name;
         $catinfo -> save();
        // $taginfo = new Tag();

     $tags= explode(",",$request->tags);
      foreach ($tags as $key => $value) {


          $taginfo =  Tag::find($value);
    
        $catinfo->tags()->attach($taginfo);
       
      }
      
    
  return redirect('admin/dashboard/category');
       
        // return view('layouts.backend.pages.category.addcategory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::find($id);
        $alltags=Tag::all();
        $tags=$categories->tags;


        return view('layouts.backend.pages.category.editcategory',compact('categories','tags','alltags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'cat_name' => 'required',
    
        ]);
        
       $this->removetags($id);
        $catinfo = Category::find($id);
        $tags= explode(",",$request->tagsid);
      foreach ($tags as $key => $value) {


          $taginfo =  Tag::find($value);
    
        $catinfo->tags()->attach($taginfo);
       
      }
        Session::flash('message', 'Success');
     $catinfo->update($request->all());
       return redirect('admin/dashboard/category');
    
     }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Category::find($id);
        $categories->tags()->detach();
        // dd($categories);
        $categories->delete();
        return back();
    }

    public function removetags($id)
    {
         $categories = Category::find($id);
        $categories->tags()->detach();  
    }

public function  createtag(Request $request)
{
       $savedtags=array();
       if($request->tag)
       {
    foreach($request->tag as $tag)
    {
      $checktag=Tag::where('name',$tag)->get();

   
        if(!count($checktag)>0)
        {

            $newtag= new Tag();
            $newtag->name=$tag;
         $newtag->save();
           $savedtags[] =$newtag;
        }
        else{
              $savedtags[]=$checktag;
        }

    }
return $savedtags;
}
   
}
}
