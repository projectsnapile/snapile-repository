<?php

namespace Snapile\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Snapile\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class AdminLoginController extends Controller
{
	public function __construct()
	{
		// $this->middleware('guest')->except(route('admin.logout'));;
	}

    public function showLoginForm()
    {
    	return view('auth.login');
    }

    public function login(Request $request)
    {
		

    	//validation
    	$this->validate($request, [
    		'username' => 'required',
    		'password' => 'required'
		]);
	
       
    	//Log the user in
    	if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
              

    		return redirect()->intended(route('admin.dashboard'));
    	}
    	return back()->withInput($request->only('username', 'remember'));
    	

    }

    public function logout(Request $request)
    {
       	Auth::logout();
    }
}
