<?php

namespace Snapile\Http\Controllers;

use Illuminate\Http\Request;
use Snapile\Http\Controllers\Controller;
use Snapile\Models\Upload;
use Snapile\Models\Category;
use Image;
use Form;
use Session;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Snapile\Models\Tag;
use Snapile\Mail\SendUploadUpdateMessage;
use Illuminate\Support\Facades\Mail;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uploads=Upload::orderBy('id', 'desc')->paginate(10);
        return view('layouts.backend.pages.imageapproval',compact('uploads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags=Tag::all();
        return view('layouts.frontend.pages.upload',compact('categories','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();

         $this->validate($request, [
            'p_title' => 'required',
            'p_desc' => 'required',
            'category_id' => 'required|integer',
            'pic' => 'required | image | mimes:jpeg,png,jpg,gif | max:80000'
        ]);

        Session::flash('message', 'Your Image has been successfully Uploaded.');

  
        $img = new Upload();
        $img -> p_title = $request -> p_title;
        $img -> p_desc = $request -> p_desc;
        $img -> category_id = $request -> category_id;
        $img -> status = "0";
        $img->user_id=Auth::user()->id;
       
         if($request->hasFile('pic')){
            $image_tmp = Input::file('pic');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(222,3453).'.'.$extension;
                $large_image_path = public_path('images/'.$filename);
                $small_image_path = public_path('images/thumbnails/'.$filename);
                $watermark = public_path('images/watermark/'.$filename);
                
                  $data= array();

                 $data['Resolution'] = Image::make($image_tmp)->exif('COMPUTED')['Width'].' X '.Image::make($image_tmp)->exif('COMPUTED')['Height'];  
                $data['Model'] = Image::make($image_tmp)->exif('Make');
                $data['SnapInfo'] = Image::make($image_tmp)->exif('UndefinedTag:0xA434');
               
              
                $exifdata = json_encode($data);
            
                // Resize Image Code
                $savedimage=Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(330,300)->save($small_image_path);

                    
                $wmimage = Image::make($image_tmp)->resize(540,433);
                $wm = Image::make(public_path('images/watermark.png'));
                $wmimage->insert($wm, 'center');
                $wmimage->save($watermark);


                // Store image name in table
                $img->image = $filename;
                $img->p_exif = $exifdata;
            }
        }

  else
  {
    $img -> image = "image.jpg";
  }
        

        $img -> save();


        $tags= explode(",",$request->tags);
      foreach ($tags as $key => $value) {


          $taginfo =  Tag::find($value);
    
        $img->tags()->attach($taginfo);
       
      }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $uploads=Upload::find($id);
       
        return view('layouts.backend.pages.imageapproval',compact('uploads'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Upload $upload)
    {
        $tags=Tag::all();
        $categories=Category::all();
        return view('layouts.frontend.pages.editupload',compact('upload','tags','categories'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Session::flash('message', 'Success');

               $img = Upload::find($id);
        $img -> p_title = $request -> p_title;
        $img -> p_desc = $request -> p_desc;
        $img -> p_cat = $request -> p_cat;

       if($request->hasFile('pic'))
  {
    $image = $request->file('pic');
    $originalname=preg_replace('/\..+$/', '', $image->getClientOriginalName());
    $filename = $originalname.time() . '.' . $image->getClientOriginalExtension();
    $location = public_path('images/' . $filename );
    Image::make($image)->resize(640,390)->save($location);

    $img->pic = $filename;
    // echo $img_tmp=Input::file('image1');
    // die;
  }
  else
  {
    $img->pic= "image.jpg";
  }

        $img -> save();
        return view('layouts.frontend.pages.upload');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
$image=Upload::find($id);
$filename=$image->image;
        $large_image_path = public_path('images/'.$filename);
         $small_image_path = public_path('images/thumbnails/'.$filename);// Value is not URL but directory file path
         $watermark = public_path('images/watermark/'.$filename);// Value is not URL but directory file path
        if(File::exists($large_image_path)) {
           

            File::delete( $large_image_path);
        }
        if(File::exists(   $small_image_path)) {
           

            File::delete(   $small_image_path);
        } 
        if(File::exists(   $watermark)) {
           

            File::delete(   $watermark);
        }



        $image->delete();
       
       return back();

        
    }



   function changestatus(Request $request)
   {
 $image=Upload::find($request->id);
 $image->status=!$image->status;
 $image->save();
 $message=$image->status==1?'Your image has been approved.':'Your approved image was unpublished due to error';
 $image->user->receiver()->create(['sender_id'=>Auth::user()->id,
      'message'=>$message,'subject'=>'upload confirmation']);
 return "success";
   }

   public function discoverpage(Upload $upload)
    {
        $uploads=Upload::where('status',1)->get();
        return view('layouts.frontend.pages.discover',compact('upload', 'uploads'));
    }

    public function sendmessage(Request $request)
    {
     
        try
        {
            Mail::to($request->email)->send(new SendUploadUpdateMessage($request->message,$request->upload,$request->name,$request->uploadeddate));
            return back();
        }
        catch (\Exception $e)
        {
            echo 'Error - '.$e;
        }
    }
    public function updateuserimage(Request $request)
    {

        $this->removetags($request->id);
        $catinfo = Upload::find($request->id);
        $tags= explode(",",$request->tagsid);
      
      foreach ($tags as $key => $value) {


          $taginfo =  Tag::find($value);
    
        $catinfo->tags()->attach($taginfo);
       
      }
        Session::flash('message', 'Success');
   
     $catinfo->update($request->all());

       return back();
    }
    
    public function removetags($id)
    {
         $upload = Upload::find($id);
        $upload->tags()->detach();  
    }

    public function search(Request $request)
    {
      if(isset($request->searchkey))
      {
     $message='';
      $upload=  Upload::Where('p_title', 'like', '%' . Input::get('searchkey') . '%')->get();
    
       $tags=Tag::Where('name', 'like', '%' . Input::get('searchkey'))->with('uploads')->get();
   
       $data = $upload->merge($tags);

       if(count($upload)==0 && count($tags)==0)
       {
        $allupload=Upload::pluck('p_title');
        $alltags=Tag::pluck('name');
        // input misspelled word
$input = $request->searchkey;

// array of words to check against
$words  = $allupload->merge($alltags);

// no shortest distance found, yet
$shortest = -1;

// loop through words to find the closest
foreach ($words as $word) {

    // calculate the distance between the input word,
    // and the current word
    $lev = levenshtein($input, $word);

    // check for an exact match
    if ($lev == 0) {

        // closest word is this one (exact match)
        $closest = $word;
        $shortest = 0;

        // break out of the loop; we've found an exact match
        break;
    }

    // if this distance is less than the next found shortest
    // distance, OR if a next shortest word has not yet been found
    if ($lev <= $shortest || $shortest < 0) {
        // set the closest match, and shortest distance
        $closest  = $word;
        $shortest = $lev;
    }
}

if ($shortest == 0) {
 $message="Exact match found: $closest\n";
} else {
 $message= "Did you mean: <a href='/image/search?searchkey=".$closest."'/>$closest?\n</a>";
}

       
        
       }

       return  view('layouts.frontend.pages.search',compact('data','message'));
      }else{
          return back();
      }
    }
}
