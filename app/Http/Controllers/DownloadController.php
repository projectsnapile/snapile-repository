<?php

namespace Snapile\Http\Controllers;

use Illuminate\Http\Request;
use Snapile\Http\Controllers\Controller;
use Snapile\Models\Upload;
use Snapile\Models\Category;
use Image;
use Form;
use Session;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Snapile\Models\Tag;
use Snapile\Mail\SendUploadUpdateMessage;
use Illuminate\Support\Facades\Mail;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $uploads = Upload::find($id);
        $tags = Tag::find($id);
        return view('layouts.frontend.pages.download',compact('uploads', 'tags'));
    }

    public function verification(Request $request)
    {
      
        //hit the khalti server
    $args = http_build_query(array(
    'token' => $request->input('trans_token'),
    'amount' => $request->input('amount')
    ));
    $url = "https://khalti.com/api/v2/payment/verify/";
    # Make the call using API.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$args);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $headers = ['Authorization: Key test_secret_key_a95e580a914742d2b884618fd5ce38c9'];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // Response
    $response = curl_exec($ch);
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
