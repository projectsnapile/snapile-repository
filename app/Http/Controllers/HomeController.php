<?php

namespace Snapile\Http\Controllers;
use Illuminate\Http\Request;
use Snapile\Http\Controllers\Controller;
use Snapile\Models\Upload;
use Snapile\Models\Category;
use Snapile\User;
use Image;
use Form;
use Session;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Snapile\Message;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(User $user)
    {
        return view('layouts/frontend/pages/portfolio');
    }

    public function profileimagestore(Request $request)
    {
         $this->validate($request, [
            'profile_photo' => 'required | image | mimes:jpeg,png,jpg,gif | max:2048',
            'cover_photo' => 'required | image | mimes:jpeg,png,jpg,gif | max:2048'
        ]);

        Session::flash('message', 'Your profile images have been uploaded.');

  
        $img = User::find($request->id);

       if($request->hasFile('profile_photo'))
  {
    $proimage = $request->file('profile_photo');
    $originalname = preg_replace('/\..+$/', '', $proimage->getClientOriginalName());
    $filename1 = $originalname.time() . '.' . $proimage->getClientOriginalExtension();
    $location = public_path('images/portfolio/profile/' . $filename1 );
    Image::make($proimage)->resize(125,125)->save($location);

    $img -> profile_photo = $filename1;
    // echo $img_tmp=Input::file('image1');
    // die;
  }
  // else
  // {
  //   $img -> profile_photo = "images/portfolio/profile/user.jpg";
  // }

  if($request->hasFile('cover_photo'))
    {
        $coverimage = $request->file('cover_photo');
        $originalname = preg_replace('/\..+$/', '', $coverimage->getClientOriginalName());
        $filename2 = $originalname.time() . '.' . $coverimage->getClientOriginalExtension();
        $location = public_path('images/portfolio/cover/' . $filename2 );
        Image::make($coverimage)->resize(1400,720)->save($location);

        $img -> cover_photo = $filename2;
    }      

    // else
    // {
    //   $img -> cover_photo = "images/portfolio/cover/cover.jpg";
    // }

        $img -> save();
        return back();
    }

    public function viewprofile($id)
    {
      $user=User::find($id);
      return view('layouts.frontend.pages.viewprofile',compact('user'));
    }

    public function usermessage(Request $request,$id)
    {
     
      Auth::user()->sender()->create(['receiver_id'=>$id,
      'message'=>$request->message,'subject'=>$request->subject]);
      return back()->with('message','Your Message Has Been Submitted');
    }

    public function updatestatus(Message $message)
    {
      $message->status=!$message->status;
      $message->save();
      return back()->with('message','Your Message Status Has Been Changed');
    }
}
