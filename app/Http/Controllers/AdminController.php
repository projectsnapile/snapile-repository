<?php

namespace Snapile\Http\Controllers;
use Snapile\Http\Controllers\Controller;    
use Illuminate\Http\Request;
use Snapile\Models\Category;
use Snapile\Models\Upload;
use Snapile\Models\Blog;
use Snapile\User;

class AdminController extends Controller
{

      public function __construct()
    {
        $this->middleware('admin');
    }

    public function dashboard()
    {
        $categorycount= Category::all()->count();
        $uploadcount= Upload::all()->count();
        $blogcount= Blog::all()->count();
        $usercount= User::where('usertype','<>', 1)->count();

        return view ('layouts.backend.pages.home',compact('categorycount','uploadcount','blogcount','usercount'));
    }

}
