<?php

namespace Snapile\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Snapile\Http\Controllers\Controller;
use Snapile\Models\Blog;
use File;
use Image;
use Form;
use Illuminate\Support\Facades\Auth;
use Session;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs=Blog::paginate(10)->sortByDesc('created_at');
        return view('layouts.backend.pages.blog.blogapproval',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.frontend.pages.portfolio');
    }

    public function admincreate()
    {
        return view('layouts.backend.pages.blog.writeblog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'b_title' => 'required',
            'b_detail' => 'required',
            'blogpic' => 'required | image | mimes:jpeg,png,jpg,gif | max:2048'
        ]);
        Session::flash('message', 'Blog Successfully Uploaded.');

  
        $img = new Blog();
        $img -> b_title = $request -> b_title;
        $img -> b_author = $request -> b_author;
        $img -> b_detail = $request -> b_detail;
        $st= Auth::user()->role_id==0?1:0;
        $img -> b_status = $st;


       if($request->hasFile('blogpic'))
  {
    $image = $request->file('blogpic');
    $originalname = preg_replace('/\..+$/', '', $image->getClientOriginalName());
    $filename = $originalname.time() . '.' . $image->getClientOriginalExtension();
    $location = public_path('images/blogs/' . $filename );
    Image::make($image)->resize(1349, 690, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
    })->save($location);

    $img -> image = $filename;
    // echo $img_tmp=Input::file('image1');
    // die;
  }
  else
  {
    $img -> image = "image.jpg";
  }
        

        $img -> save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blogs=Blog::find($id);
        return view('layouts.backend.pages.blog.blogapproval',compact('blogs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogs = Blog::find($id);
        return view('layouts.backend.pages.blog.editblog',compact('blogs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'b_title' => 'required',
            'b_detail' => 'required',
            'blogpic' => 'required | image | mimes:jpeg,png,jpg,gif | max:2048'
        ]);
        Session::flash('message', 'The Blog has been updated.');

  
        $img = Blog::find($id);
        $img -> b_title = $request -> b_title;
        $img -> b_detail = $request -> b_detail;

        if ($request->hasFile('blogpic')) {
            //add the new photo
            $image = $request->file('blogpic');
            $originalname = preg_replace('/\..+$/', '', $image->getClientOriginalName());
            $filename = $originalname.time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/blogs/' . $filename );
            Image::make($image)->resize(1349, 690, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($location);
            $oldFilename = $img->image;
            //update the database
            $img->image = $filename;
            //Delete the old photo
            if(File::exists($oldFilename)) {
               

                File::delete( $oldFilename);
            }
        }
        $img->save();
        return redirect('admin/dashboard/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image=Blog::find($id);
        $filename=$image->image;
        $image_path = public_path('images/blogs/'.$filename);
            if(File::exists($image_path)) {
               

                File::delete( $image_path);
            }
           
        $image->delete();
    return back();

    }

   public function blogstatus(Request $request)
       {
     $image=Blog::find($request->id);
     $image->b_status=!$image->b_status;
     $image->save();
     return "success";
   }

   

   public function blogdetail(Blog $blog)
   {
        $blogs=Blog::all();
        return view('layouts.frontend.pages.blog-detail',compact('blog','blogs'));
   }

    public function blogpage(Blog $blog)
    {
        $blogs=Blog::where('b_status',1)->orderBy('created_at', 'DESC')->get();
        return view('layouts.frontend.pages.blog',compact('blog', 'blogs'));
    }
}
