<?php

namespace Snapile\Http\Middleware;
use Snapile\Role;

use Closure;
use Illuminate\Support\Facades\Auth;
class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $request->user();
        // $role = new Role;
        // $userRoles = $role->pluck('name');
        // if(!$userRoles->contains('admin')) {
        //     return redirect(route('admin.login'));
        // }     
        
    if(Auth::check())
        if (Auth::user()->usertype==1) {
            return $next($request);
        }
        return redirect('login');
    }
}
