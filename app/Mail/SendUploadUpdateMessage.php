<?php

namespace Snapile\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUploadUpdateMessage extends Mailable
{
    protected $message;
    protected $upload;
    protected $name;
    protected $uploadeddate;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message,$upload,$name,$uploadeddate)
    {
        $this->message=$message;
        $this->upload=$upload;
        $this->name=$name;
        $this->uploadeddate=$uploadeddate;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('uploads.senduploadupdatemessage')->with('message',$this->message)
        ->with('name',$this->name)->with('upload',$this->upload)->with('uploadeddate',$this->uploadeddate);
    }
}
