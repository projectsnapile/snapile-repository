<?php

namespace Snapile\Models;

use Illuminate\Database\Eloquent\Model;
use Snapile\User;

class Upload extends Model
{
    protected $table = 'uploads';
    protected $with=['user'];
    protected $fillable = ['id', 'p_title', 'p_desc', 'category_id', 'created at', 'updated_at'];
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function tags()
    {
    	return $this->belongsToMany('Snapile\Models\Tag','tags_uploads');
    }

}
