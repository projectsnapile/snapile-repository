<?php

namespace Snapile\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
     protected $table = 'tags';
     protected $fillable=['name'];
     protected $with=['categories','uploads'];

     public function categories()
     {
     	return $this->belongsToMany('Snapile\Models\Category','categories_tags');
     }
     public function uploads()
     {
     	return $this->belongsToMany('Snapile\Models\Upload','tags_uploads');
     }
}
