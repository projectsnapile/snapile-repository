<?php

namespace Snapile\Models;

use Illuminate\Database\Eloquent\Model;
use Snapile\User;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $fillable = ['b_title', 'b_detail', 'b_author'];

    public function user()
    {
        return $this->belongsTo(User::class,'b_author');
    }
}
