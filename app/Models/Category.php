<?php

namespace Snapile\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable=['cat_name'];

    public function tags()
    {
    	return $this->belongsToMany('Snapile\Models\Tag','categories_tags');
    }

}
