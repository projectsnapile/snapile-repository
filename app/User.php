<?php

namespace Snapile;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Snapile\Models\Blog;
use Snapile\Models\Upload;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'fname', 'lname', 'email', 'username', 'role_id' ,'password', 'usertype','address1','address2',
];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     public function roles()
     {
        return $this->belongsToMany('Snapile\Role','role_user');
     }
     public function uploads()
     {
         return $this->hasMany(Upload::class,'user_id');
     }
     public function getNameAttribute($name)
     {
         return $this->attributes['fname']. " ".$this->attributes['lname'];
 
     }
 
     protected $with=['receiver','sender'];
     public function receiver()
     {
         return $this->hasMany(Message::class,'receiver_id')->latest();
     }
     public function sender()
     {
         return $this->hasMany(Message::class,'sender_id')->latest();
     }
     public function messagecount()
     {
         return $this->hasMany(Message::class,'receiver_id')->where('status',0);
     }
     public function blog()
     {
         return $this->hasMany(Blog::class,'b_author')->latest();
     }
    }
  
