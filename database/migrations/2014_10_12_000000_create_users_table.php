<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('email')->unique();
            $table->string('usertype');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('profile_photo')->default('user.jpg');
            $table->string('cover_photo')->default('cover.png');
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('email_verified_at')->nullable();
            $table->unsignedBigInteger('role_id');
        //   $table->foreign('role_id')->references('id')->on('role_user')->onCascade('delete');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
