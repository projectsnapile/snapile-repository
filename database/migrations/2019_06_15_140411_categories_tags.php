<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoriesTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('categories_tags', function(Blueprint $table ){
  $table->unsignedBigInteger('category_id');
  $table->foreign('category_id')->references('id')->on('categories');
  $table->unsignedBigInteger('tag_id');
  $table->foreign('tag_id')->references('id')->on('tags');
});
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
