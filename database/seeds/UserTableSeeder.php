<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 DB::table('users')->insert([

            'fname' => 'Admin',
            'lname'=>'admin',
            'username'=>'admin',
            'email'=>'admin@admin.com',
            'role_id'=>0,
            'usertype'=>1,
            'password'=>bcrypt('admin'),
            'profile_photo'=>'1.png',
            'cover_photo'=>'2.png',
            'address1'=>'test',
            'address2'=>'test'
        ]);
    	 DB::table('users')->insert([

            'fname' => 'User',
            'lname'=>'user',
            'username'=>'user',
            'email'=>'user@user.com',
            'usertype'=>2,
            'password'=>bcrypt('user'),
            'profile_photo'=>'1.png',
            'cover_photo'=>'2.png',
            'address1'=>'test',
            'address2'=>'test'
        ]);
        //
    }
}
